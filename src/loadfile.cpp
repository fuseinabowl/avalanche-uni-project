#include "loadfile.h"

#include <fstream>
#include <sstream>

namespace Avalanche
{
  // TODO make this throw errors if it can't open a file
  std::vector<char> loadFile(const std::string& filePath) throw (FileAccessException)
  {
    std::vector<char> out;
    std::ifstream readingFile(filePath, std::ios::ate);
    if (readingFile.good())
    {
      out.resize(readingFile.tellg());
      readingFile.seekg(0);
      readingFile.read(out.data(), out.size());

      return std::move(out);
    }
    else
    {
      // throw error
      throw FileAccessException(filePath);
    }
  }

  FileAccessException::FileAccessException(const std::string& fileName)
  {
    std::stringstream whatStream;
    whatStream
      << "could not open file \"" << fileName << "\"";
    this->exceptionString = whatStream.str();
  }

  FileAccessException::~FileAccessException() noexcept
  {
  }

  const char* FileAccessException::what() noexcept
  {
    return this->exceptionString.c_str();
  }
}
