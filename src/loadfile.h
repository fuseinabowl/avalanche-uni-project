#ifndef _LOADFILE_H_
#define _LOADFILE_H_

#include <vector>
#include <string>
#include <exception>

// pass by ref, don't want to copy a file around memory
namespace Avalanche
{
  class FileAccessException : public std::exception
  {
    std::string exceptionString;
   public:
    FileAccessException(const std::string& filename);
    ~FileAccessException() noexcept;
    const char* what() noexcept;
  };

  std::vector<char> loadFile(const std::string& fileName) throw (FileAccessException);
}

#endif
