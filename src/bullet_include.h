#ifndef _AVALANCHE_BULLET_H_
#define _AVALANCHE_BULLET_H_

// hide some problems (warnings only) with Bullet in GCC
// to allow easy error reading in my own code
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <btBulletDynamicsCommon.h>
#pragma GCC diagnostic pop

#endif
