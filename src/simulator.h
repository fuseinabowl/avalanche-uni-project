#ifndef _AVALANCHE_SIMULATOR_H_
#define _AVALANCHE_SIMULATOR_H_

#include "physics.h"
#include <vector>
#include "graphics.h"
#include "openGL.h"

namespace Avalanche
{
  class Simulator
  {
    GraphicsEngine graphics;
    //PhysicsEngine physics;

    // you need to construct the Landscape before calling this so it can be passed to both the engines used
    Simulator(const Landscape::Landscape&, const GLuint groundTextureUnitName) noexcept;
    // starts the app, sets the simulator in motion, returns when the user closes the app
    void run() noexcept;
   public:
    typedef void Results; // can create a class that will hold results of the simulation if I want to

    // public entries to run the simulator, return when the user closes the app
    static Results PanoramaSimulator(float latitude, float longitude, float latSpan, float longSpan); // panorama constructor
    static Results MeshSimulator(unsigned int rowWidth, std::vector<float>&& meshData); // supplied mesh constructor
    static Results ProcGenSimulator(unsigned int width, unsigned int height); // procedurally generated mesh constructor

    // non-moveable, non-copyable
    Simulator(const Simulator&) = delete;
    Simulator& operator=(const Simulator&) = delete;
  };
}

#endif 
