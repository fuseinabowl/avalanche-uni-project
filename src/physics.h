#ifndef _AVALANCHE_PHYSICS_H_
#define _AVALANCHE_PHYSICS_H_

//#include "landscape/landscape.h"
#include "graphics.h"

#include <vector>
#include <memory>
#include <utility>

#include "bullet_include.h"
#include "landscape/landscape.h"

namespace Avalanche
{
  class PhysicsEngine
  {
    // because of the tight coupling between physics and graphics (see Bullet motion states and interpolation)
    // I have access to the graphics engine here too
    GraphicsEngine* graphics;

    // Bullet data

    // world constructor data
    btDefaultCollisionConfiguration collisionConfiguration;
    btCollisionDispatcher collisionDispatcher;
    btDbvtBroadphase broadphase; // TODO: research and select broadphase
    btSequentialImpulseConstraintSolver constraintSolver; // TODO: maybe replace with parallel solver
    // actual world
    btDiscreteDynamicsWorld world;

    std::unique_ptr<btStridingMeshInterface> landscapeMesh;
    btBvhTriangleMeshShape landscapeShape; // the landscape mesh in bullet form
    btDefaultMotionState landscapeMotionState; // only need a default one as landscape will not move so don't need to update the position in graphics
    btRigidBody landscapeBody; // a body using the landscape mesh

    std::vector<std::pair<std::unique_ptr<btRigidBody>, std::shared_ptr<btCollisionShape> > > coreBlocks; // bodies for snow core or debris, TODO maybe add motion states here so that they will destruct the objects in graphics too?
    std::vector<std::weak_ptr<btCollisionShape> > coreBlockShapes; // shapes already available that are currently in use by the objects in the previous vector (allows easy reuse of shapes)

    // Navier-Stokes data

    // debug drawer (externally memory managed, this class will not delete it when it's done)
    btIDebugDraw* debugDrawer;
   public:
    PhysicsEngine(GraphicsEngine&, const Landscape::Landscape&); // TODO: add start position of fracture or core blocks
    void step(float timeSinceLastCall); // variable step size as Bullet does interpolation for rendering

    // debug helpers
    void setDebugDrawer(btIDebugDraw&);
    void debugDraw();
  };
}

#endif
