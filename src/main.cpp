#include "main.h"
#include <iostream>

#include "simulator.h"

int main(int argc, char** argv)
{
  std::cout << "hello, there are " << argc << " parameters, they read " << *argv;

  for (int currentStringIndex = 1; argc > currentStringIndex; ++currentStringIndex)
  {
    std::cout << ", " << argv[currentStringIndex];
  }

  std::cout << std::endl;

  Avalanche::Simulator::ProcGenSimulator(5,5);
}

