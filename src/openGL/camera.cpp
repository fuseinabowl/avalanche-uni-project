#include "camera.h"
#include <algorithm>
#include <complex>
#include <iostream>

namespace Avalanche
{
  namespace OpenGL
  {
    std::array<GLfloat, 3> Camera::normalize(const std::array<GLfloat, 3>& in)
    {
      std::array<GLfloat, 3> out;
      GLfloat normalizer = in[0] * in[0] + in[1] * in[1] + in[2] * in[2];
      normalizer = std::sqrt(normalizer);
      for (unsigned char index = 0; index < in.size(); ++index)
      {
        out[index] = in[index] / normalizer;
      }

      return std::move(out);
    }

    void Camera::position(const std::array<GLfloat, 3>& newPosition, const std::array<GLfloat, 3>& direction, const std::array<GLfloat, 3>& up)
    {
      // construct the new modelView matrix from the parameters
      // no need to alter the projection matrix
      auto normalized = normalize(direction);
      this->modelView[2] = {{normalized[0], normalized[1], normalized[2], 0}};

      // cross the up and the z axis
      const auto& zAxis = this->modelView[2];
      normalized = {{
        up[1] * zAxis[2] - up[2] * zAxis[1],
        up[2] * zAxis[0] - up[0] * zAxis[2],
        up[0] * zAxis[1] - up[1] * zAxis[0]}};
      normalized = normalize(normalized);

      std::copy(normalized.begin(), normalized.end(), this->modelView[0].begin());
      this->modelView[0][3] = 0.0f;

      // cross product the two previous vectors
      const auto& xAxis = this->modelView[0];
      this->modelView[1] = {{
        zAxis[1] * xAxis[2] - zAxis[2] * xAxis[1],
        zAxis[2] * xAxis[0] - zAxis[0] * xAxis[2],
        zAxis[0] * xAxis[1] - zAxis[1] * xAxis[0],
        0}};

      // homogeneous column
      this->modelView[3] = {{0,0,0,1}};

      // position translation in the homogeneous row
      for (unsigned i = 0; i < 3; ++i)
      {
        auto& currentRow = this->modelView[i];

        currentRow[3] = -(
          currentRow[0] * newPosition[0] + 
          currentRow[1] * newPosition[1] +
          currentRow[2] * newPosition[2]);
      }

      // copy them all into the normal matrix
      for (unsigned char index = 0; index < 3; ++index)
      {
        std::copy(this->modelView[index].begin(), this->modelView[index].begin() + 2, this->normal[index].begin());
      }
    }

    void Camera::frustum(GLfloat FOVx, GLfloat FOVy, GLfloat near, GLfloat far)
    {
      this->projection[0] = {{near / FOVx, 0, 0, 0}};
      this->projection[1] = {{0, near / FOVy, 0, 0}};
      this->projection[2] = {{0, 0, (- far - near) / (near - far), -1}};
      this->projection[3] = {{0, 0, -2 * (far * near) / (near - far), 0}};
    }

    void Camera::uploadModelView(GLint location) const
    {
      glUniformMatrix4fv(location, 1, GL_FALSE, reinterpret_cast<const GLfloat*>(this->modelView.data()));
    }

    void Camera::uploadProjection(GLint location) const
    {
      glUniformMatrix4fv(location, 1, GL_FALSE, reinterpret_cast<const GLfloat*>(this->projection.data()));
    }

    void Camera::uploadNormal(GLint location) const
    {
      glUniformMatrix3fv(location, 1, GL_FALSE, reinterpret_cast<const GLfloat*>(this->normal.data()));
    }
  }
}
