#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "openGL.h"
#include <array>

namespace Avalanche
{
  namespace OpenGL
  {
    class Camera
    {
      std::array<std::array<GLfloat, 4>, 4>
        modelView,
        projection;
      std::array<std::array<GLfloat, 3>, 3>
        normal;

      static std::array<GLfloat, 3> normalize(const std::array<GLfloat, 3>&);
     public:
      // modifiers
      // changing the camera location
      void position(const std::array<GLfloat, 3>& newPosition, const std::array<GLfloat, 3>&direction, const std::array<GLfloat, 3>& up);
      // changing the camera projection
      void frustum(GLfloat FOVx, GLfloat FOVy, GLfloat near, GLfloat far);

      // uniform uploads
      void uploadModelView(GLint uniformLocation) const;
      void uploadProjection(GLint uniformLocation) const;
      void uploadNormal(GLint uniformLocation) const;
    };
  }
}

#endif
