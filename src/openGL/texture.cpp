#include "texture.h"
#include <iostream>

namespace Avalanche
{
  namespace OpenGL
  {
    VolumeTexture::VolumeTexture() :
      texName(0)
    {
    }

    VolumeTexture::VolumeTexture(const std::vector<std::array<GLfloat, 4> >& data, unsigned width, unsigned height) :
      width(width), height(height),
      depth(data.size() / (width * height))
    {
      glGenTextures(1, &this->texName);
      glBindTexture(GL_TEXTURE_3D, this->texName);
      glTexImage3D(
          GL_TEXTURE_3D, 0, GL_RGBA16F,
          this->width, this->height, this->depth, 0,
          GL_RGBA, GL_FLOAT, data.data()
            );

      VolumeTexture::applyTextureParameters();
    }

    VolumeTexture::VolumeTexture(unsigned width, unsigned height, unsigned depth) :
      width(width), height(height), depth(depth)
    {
      glGenTextures(1, &this->texName);
      glBindTexture(GL_TEXTURE_3D, this->texName);
      glTexImage3D(
          GL_TEXTURE_3D, 0, GL_RGBA16F,
          this->width, this->height, this->depth, 0,
          GL_RGBA, GL_FLOAT, nullptr
            );

      VolumeTexture::applyTextureParameters();
    }

    void VolumeTexture::applyTextureParameters()
    {
      glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R,     GL_CLAMP_TO_EDGE);
    }

    VolumeTexture::~VolumeTexture()
    {
      glDeleteTextures(1, &this->texName);
    }

    VolumeTexture::VolumeTexture(VolumeTexture&& moveFrom) :
      texName(moveFrom.texName),
      width(moveFrom.width),
      height(moveFrom.height),
      depth(moveFrom.depth)
    {
      moveFrom.texName = 0;
      moveFrom.width = moveFrom.height = moveFrom.depth = 0;
    }

    VolumeTexture& VolumeTexture::operator=(VolumeTexture&& rhs)
    {
      glDeleteTextures(1, &this->texName);

      this->texName = rhs.texName;
      this->width = rhs.width;
      this->height = rhs.height;
      this->depth = rhs.depth;

      rhs.texName = 0;
      rhs.width = rhs.height = rhs.depth = 0;

      return *this;
    }

    GLuint VolumeTexture::getName()
    {
      return this->texName;
    }

    void VolumeTexture::bind() const
    {
      glBindTexture(GL_TEXTURE_3D, this->texName);
    }

    std::array<GLuint, 3> VolumeTexture::getDimensions() const
    {
      return {{this->width, this->height, this->depth}};
    }
  }
}
