#ifndef _SHADER_FWD_H_
#define _SHADER_FWD_H_

namespace Avalanche
{
  namespace OpenGL
  {
    // determine which openGL upload function to use by template param
    template <class UploadClass>
      void Shader::upload(const std::string& uniformName, const UploadClass&)
    {
      // buffer it for use with the next call to draw
    }

    template <class UploadClass>
      void Shader::uploadImmediate(const std::string& uniformName, const UploadClass&)
    {
    }

    template <class Command>
      void Shader::draw(const Mesh& mesh, const std::vector<Command>& separatingCommands)
    {
      this->activate();
      // call draw on the mesh
      mesh.draw(separatingCommands);
    }
  }
}

#endif
