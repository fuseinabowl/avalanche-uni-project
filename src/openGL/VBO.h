#ifndef _VBO_H_
#define _VBO_H_

#include "openGL.h"
#include <complex>
#include <vector>
#include <deque>
#include <array>
#include <boost/fusion/algorithm.hpp>
#include "attribute.h"
#include <stdexcept>

namespace Avalanche
{
  namespace OpenGL
  {
    class Shader;

    class Mesh // abstract
    {
     public:
      // class users must implement these
      // can also be used by users for more low level interaction with the VBOs
      virtual void setUp() const = 0;
      virtual void draw() const = 0;
      virtual void tearDown() const = 0;
     public:
      Mesh();
      virtual ~Mesh();
      // draws the number of times as the size of inbetweenCommands.size() + 1
      // the first draw will be done immediately after setting up the VBO
      // it will access inbetweenCommands[number of times drawn] and call operator()
      // do not call another Mesh::draw in the commands
      // do not change the openGL context in the commands
      // other openGL commands can be run
      template <class T>
        void draw(const std::vector<T>& inbetweenCommands);
    };

    class UnitSquare : public Mesh
    {
      GLuint bufferName;
      typedef GLfloat UploadType;
      typedef std::array<UploadType, 2> Vertex;
      static const GLsizei stride = sizeof(Vertex);
      static const GLenum elementType = GL_FLOAT;
      static const GLuint elementCount = 4;
     public:
      void setUp() const;
      void draw() const;
      void tearDown() const;
     public:
      UnitSquare();
    };

    class TriangleStrip : public Mesh
    {
      enum bufferStore{
        DataAddress,
        ElementAddress
          };
      std::array<GLuint, 2> bufferNames;
      GLint elementCount;

      static const GLushort resetIndexValue;
     public:
      typedef std::array<GLfloat, 8> Vertex;
      typedef std::deque<Vertex> VertexVector;
      TriangleStrip(const VertexVector& data, unsigned width) throw (std::logic_error);
      
      TriangleStrip(const TriangleStrip&) = delete;
      TriangleStrip& operator=(const TriangleStrip&) = delete;

      TriangleStrip(TriangleStrip&&);
      TriangleStrip& operator=(TriangleStrip&&);

      ~TriangleStrip();

      void setUp() const;
      void draw() const;
      void tearDown() const;
    };
  }
}

#include "VBO_fwd.h"

#endif
