#include "VBO.h"
#include <tuple>
#include <iostream>
#include <sstream>
#include <limits>

namespace Avalanche
{
  namespace OpenGL
  {
    Mesh::Mesh()
    {
    }

    Mesh::~Mesh()
    {
    }

    UnitSquare::UnitSquare()
    {
      // generate the data to be uploaded
      std::array<Vertex, UnitSquare::elementCount> squareVertices
        {{ {{0,0}}, {{1,0}}, {{1,1}}, {{0,1}} }};
      // construct the buffers
      glGenBuffers(1, &this->bufferName);
      // bind and upload the buffers
      glBindBuffer(GL_ARRAY_BUFFER, this->bufferName);
      glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * squareVertices.size(), squareVertices.data(), GL_STATIC_DRAW);
    }

    void UnitSquare::setUp() const
    {
      // bind the buffers
      glBindBuffer(GL_ARRAY_BUFFER, this->bufferName);
      // assign the pointers
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 2, UnitSquare::elementType, GL_FALSE, UnitSquare::stride, nullptr);
    }

    void UnitSquare::draw() const
    {
      glDrawArrays(GL_QUADS, 0, 4);
    }

    void UnitSquare::tearDown() const
    {
    }

    const GLushort TriangleStrip::resetIndexValue = std::numeric_limits<GLushort>::max();

    TriangleStrip::TriangleStrip(const std::deque<std::array<GLfloat, 8> >& data, unsigned width) throw (std::logic_error) :
      elementCount((data.size() / width - 1) * (width * 2 + 1))
    {
      // work out the height of the map
      unsigned height = data.size() / width;
      if (data.size() % width)
      {
        std::stringstream bad;
        bad << "data size (" << data.size() << ") is not completely rectangular with width (" << width << ")";
        throw std::logic_error(bad.str());
      }

      // construct the buffers
      glGenBuffers(2, bufferNames.data());
      glBindBuffer(GL_ARRAY_BUFFER, bufferNames[TriangleStrip::DataAddress]);
      glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(TriangleStrip::VertexVector::value_type), nullptr, GL_STATIC_DRAW);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferNames[TriangleStrip::ElementAddress]);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, (height - 1) * (2 * width + 1) * sizeof(GLushort), nullptr, GL_STATIC_DRAW);

      // upload the vertex data
      // get the biggest vertex upload you can allocate
      // start at the size of the deque
      std::vector<Vertex> vertexUpload;
      std::vector<Vertex>::size_type uploadSize = data.size();
      while(uploadSize > 0)
      {
        try
        {
          vertexUpload.resize(uploadSize);
          break;
        }
        catch (std::bad_alloc&)
        {
          std::cerr << "not uploading the whole thing!" << std::endl;
          // continue with the loop
          uploadSize /= 2;
        }
      }

      if (0 == uploadSize)
      {
        // we can't upload any data?!?
        throw std::bad_alloc();
      }

      // upload the deque in chunks
      auto uploadIterator = data.begin();
      for (; uploadIterator + uploadSize < data.end(); uploadIterator += uploadSize)
      {
        std::copy(uploadIterator, uploadIterator + uploadSize, vertexUpload.begin());
        glBufferSubData(GL_ARRAY_BUFFER, std::distance(data.begin(), uploadIterator) * sizeof(TriangleStrip::Vertex), vertexUpload.size() * sizeof(TriangleStrip::Vertex), vertexUpload.data());
      }

      // upload the final chunk
      vertexUpload.resize(std::distance(uploadIterator, data.end()));
      std::copy(uploadIterator, data.end(), vertexUpload.begin());
      glBufferSubData(GL_ARRAY_BUFFER, std::distance(data.begin(), uploadIterator) * sizeof(TriangleStrip::Vertex), vertexUpload.size() * sizeof(TriangleStrip::Vertex), vertexUpload.data());

      // construct a set of triangle strips that covers the mesh
      std::vector<GLushort> meshIndices(2 * width + 1);
      for (unsigned y = 1; y < height; ++y) // there are one less row of triangles than rows of vertices
      {
        for (unsigned x = 0; x < width; ++x)
        {
          // the two triangles at x in this row, from top to bottom
          meshIndices.at(x * 2)     = (x + width * (y-1));
          meshIndices.at(x * 2 + 1) = (x + width * (y  ));
        }

        // reset element
        meshIndices.back() = TriangleStrip::resetIndexValue;

        // upload this mesh to the element buffer
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, (y - 1) * (2 * width + 1) * sizeof(GLushort), meshIndices.size() * sizeof(GLushort), meshIndices.data());
      }
    }

    TriangleStrip::~TriangleStrip()
    {
      glDeleteBuffers(2, this->bufferNames.data());
    }

    void TriangleStrip::setUp() const 
    {
      // choose the VBO as the source
      glBindBuffer(GL_ARRAY_BUFFER, this->bufferNames[0]);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->bufferNames[1]);

      // enable the position and the normal map attribute locations
      glEnableVertexAttribArray(0);
      glEnableVertexAttribArray(1);

      // select the attribute locations
      glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(TriangleStrip::Vertex), nullptr);
      glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(TriangleStrip::Vertex), (GLfloat*)nullptr + 4);

      // enable and select element reset index
      glEnable(GL_PRIMITIVE_RESTART);
      glPrimitiveRestartIndex(TriangleStrip::resetIndexValue);
    }

    void TriangleStrip::draw() const 
    {
      // draw the mesh
      glDrawElements(GL_TRIANGLE_STRIP, this->elementCount, GL_UNSIGNED_SHORT, 0);
    }

    void TriangleStrip::tearDown() const
    {
      // disable reset index
      glDisable(GL_PRIMITIVE_RESTART);
      // turn off attribute locations
      glDisableVertexAttribArray(0);
      glDisableVertexAttribArray(1);
    }
  }
}
