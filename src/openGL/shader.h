#ifndef _SHADER_H_
#define _SHADER_H_

#include "openGL.h"
#include "VBO.h"
#include <vector>
#include <utility>
#include <string>
#include <unordered_map>
#include <memory>

namespace Avalanche
{
  namespace OpenGL
  {
    class Shader
    {
      public:
      GLuint programName;
      std::unordered_map<std::string, GLuint> uniformMap;

      class Upload
      {
       public:
        virtual void operator()() = 0;
      };

      std::vector<std::unique_ptr<Upload> > uploadBuffer;
     public:
      Shader(const std::vector<std::pair<GLenum, const std::vector<std::vector<GLchar> >&> >& shaders);
      Shader();
      ~Shader();

      Shader(const Shader&) = delete;
      Shader& operator=(const Shader&) = delete;
      Shader(Shader&&);
      Shader& operator=(Shader&&);

    // !! unimplemented !!
      // uploads on next activate or draw call
      template <class UploadClass>
        void upload(const std::string&, const UploadClass&);
      // uploads immediately, you need to make sure this shader was the last activated
      template <class UploadClass>
        void uploadImmediate(const std::string&, const UploadClass&);
    // !! end unimplemented !!
      GLint getLocation(const std::string&);

      // shortcut to draw a mesh using just this shader
      template <class Command>
        void draw(const Mesh&, const std::vector<Command>& separatingCommands = std::vector<Command>());
      void activate(); // more low level command, for use in commands for mesh::draws

      // some helper functions when creating shader strings
      static std::vector<GLchar> toCharVector(const std::string& source);
      static std::vector<GLchar> terminate(std::vector<GLchar>&& source);
    };
  }
}

#include "shader_fwd.h"

#endif
