#include "texture.h"
#include <iostream>

namespace Avalanche
{
  namespace OpenGL
  {
    template <GLenum iF>
      TextureTemplate<iF>::TextureTemplate() :
        texName(0),
        width(0), height(0)
    {
    }

    template <GLenum internalFormat>
      TextureTemplate<internalFormat>::TextureTemplate(const std::vector<std::array<GLfloat, 4> >& data, unsigned width) :
        width(width), height(data.size() / width)
    {
      glGenTextures(1, &this->texName);
      glBindTexture(GL_TEXTURE_2D, this->texName);
      glTexImage2D(
          GL_TEXTURE_2D, 0, internalFormat,
          this->width, this->height, 0,
          GL_RGBA, GL_FLOAT, data.data()
            );
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
    }

    template <GLenum iF>
      TextureTemplate<iF>::~TextureTemplate()
    {
      glDeleteTextures(1, &this->texName);
    }

    template <GLenum iF>
      TextureTemplate<iF>::TextureTemplate(TextureTemplate<iF>&& moveFrom) :
        texName(moveFrom.texName),
        width(moveFrom.width),
        height(moveFrom.height)
    {
      moveFrom.texName = 0;
      moveFrom.width = moveFrom.height = 0;
    }

    template <GLenum iF>
      TextureTemplate<iF>& TextureTemplate<iF>::operator=(TextureTemplate<iF>&& rhs)
    {
      glDeleteTextures(1, &this->texName);

      this->texName = rhs.texName;
      this->width = rhs.width;
      this->height = rhs.height;

      rhs.texName = 0;
      rhs.width = rhs.height = 0;

      return *this;
    }

    template <GLenum iF>
      GLuint TextureTemplate<iF>::getName()
    {
      return this->texName;
    }

    template <GLenum iF>
      void TextureTemplate<iF>::bind() const
    {
      glBindTexture(GL_TEXTURE_2D, this->texName);
    }

    template <GLenum iF>
      std::array<GLuint, 2> TextureTemplate<iF>::getDimensions() const
    {
      return {{this->width, this->height}};
    }
  }
}
