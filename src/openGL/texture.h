#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "openGL.h"
#include <vector>
#include <array>

namespace Avalanche
{
  namespace OpenGL
  {
    class VolumeTexture
    {
      GLuint texName;
      GLuint width, height, depth;

      static void applyTextureParameters();
     public:
      // derives the depth for you
      VolumeTexture();
      VolumeTexture(const std::vector<std::array<GLfloat, 4> >& data, unsigned width, unsigned height);
      VolumeTexture(unsigned width, unsigned height, unsigned depth); // dataless constructor
      ~VolumeTexture();

      VolumeTexture(const VolumeTexture&) = delete;
      VolumeTexture operator=(const VolumeTexture&) = delete;
      VolumeTexture(VolumeTexture&&);
      VolumeTexture& operator=(VolumeTexture&&);

      GLuint getName();
      void bind() const; // binds this texture to the currently activated texture unit

      std::array<GLuint, 3> getDimensions() const;
    };

    template <GLenum internalFormat>
      class TextureTemplate
    {
      GLuint texName;
      GLuint width, height;
     public:
      TextureTemplate();
      TextureTemplate(const std::vector<std::array<GLfloat, 4> >& data, unsigned width);
      ~TextureTemplate();

      TextureTemplate(const TextureTemplate<internalFormat>&) = delete;
      TextureTemplate operator=(const TextureTemplate<internalFormat>&) = delete;
      TextureTemplate(TextureTemplate<internalFormat>&&);
      TextureTemplate& operator=(TextureTemplate<internalFormat>&&);

      GLuint getName();
      void bind() const; // binds this texture to the currently activated texture unit

      std::array<GLuint, 2> getDimensions() const;
    };

    typedef TextureTemplate<GL_RGBA16F> NormalMap;
    typedef TextureTemplate<GL_RGBA> Texture;
  }
}

#include "texture2d_fwd.h"

#endif
