#ifndef _ATTRIBUTE_H_
#define _ATTRIBUTE_H_

// this file contains objects used in attribute uploads
// it also houses the database for the attribute traits

namespace Avalanche
{
  namespace OpenGL
  {
    namespace Attribute
    {
      template <class GLtype, GLenum type, unsigned _count>
        struct GL_trait_helper
      {
        enum {
          type_enum = type,
          count = _count,
          size = sizeof(GLtype),
        };
      };

      template <class attributeType>
        struct Attribute_traits;
      /* structure:
      {
        typedef <your trait class here> value_type;
      };*/

      // the database of all used attributes in this simulation
      // add more attributes if you start using them
      template<> struct Attribute_traits<GLfloat>
      {typedef GL_trait_helper<GLfloat, GL_FLOAT, 1> value_type;};
      template<> struct Attribute_traits<std::array<GLfloat, 2> >
      {typedef GL_trait_helper<std::array<GLfloat, 2>, GL_FLOAT, 2> value_type;};
      template<> struct Attribute_traits<std::array<GLfloat, 3> >
      {typedef GL_trait_helper<std::array<GLfloat, 3>, GL_FLOAT, 3> value_type;};
      template<> struct Attribute_traits<std::array<GLfloat, 4> >
      {typedef GL_trait_helper<std::array<GLfloat, 4>, GL_FLOAT, 4> value_type;};

      class Attribute
      {
        GLenum type_enum;
        GLuint count;
        GLuint size;
       public:
        template <class uploadType>
          Attribute create();
        GLenum getType() const;
        GLuint getCount() const;
        GLuint getSize() const;
      };

      template <class uploadType>
        Attribute Attribute::create()
      {
        return Attribute{
            type_enum(Attribute_traits<uploadType>::type_enum),
            count(Attribute_traits<uploadType>::count),
            size(Attribute_traits<uploadType>::size)
          };
      }
    }
  }
}

#endif
