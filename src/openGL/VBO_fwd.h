#ifndef _VBO_FWD_H_
#define _VBO_FWD_H_

namespace Avalanche
{
  namespace OpenGL
  {
    template <class T>
      void Mesh::draw(const std::vector<T>& inbetweenCommands)
    {
      this->setUp();
      this->draw();

      for (T& currentCommand : inbetweenCommands)
      {
        currentCommand();
        this->draw();
      }

      this->tearDown();
    }
  }
}

#endif
