#include "shader.h"
#include <iostream>
#include "loadfile.h"
#include <algorithm>

namespace Avalanche
{
  namespace OpenGL
  {
    Shader::Shader() : // null constructor
      programName(0)
    {
    }

    Shader::Shader(const std::vector<std::pair<GLenum, const std::vector<std::vector<GLchar> >&> >& shaders)
    {
      GLint compilationStatus = GL_FALSE;
      typedef std::vector<std::vector<char> > source_type_vector;
      source_type_vector sourceStrings;
      source_type_vector::value_type currentSource;
      std::vector<const GLchar*> sourceUpload;

      std::vector<GLuint> shaderNames;

      GLuint curShader;

      for (const auto& currentShaderDetails : shaders)
      {
        curShader = glCreateShader(currentShaderDetails.first);

        sourceUpload.clear();
        for (const auto& source : currentShaderDetails.second)
        {
          sourceUpload.emplace_back(source.data());
        }

        glShaderSource(curShader, sourceUpload.size(), sourceUpload.data(), nullptr);
        glCompileShader(curShader);

        glGetShaderiv(curShader, GL_COMPILE_STATUS, &compilationStatus);
        if (compilationStatus != GL_TRUE)
        {
          // failure to compile, tell me what I did wrong
          GLint errorLogSize = 0;
          glGetShaderiv(curShader, GL_INFO_LOG_LENGTH, &errorLogSize);
          std::vector<GLchar> errorLog(errorLogSize);
          glGetShaderInfoLog(curShader, errorLog.size(), nullptr, errorLog.data());

          std::cerr
            << "shader" << std::endl << "---" << std::endl;

          for (const auto& source : currentShaderDetails.second)
          {
            std::cerr << source.data();
          };

          std::cerr << std::endl << "---" << std::endl << " failed to compile:" << std::endl << 
            errorLog.data() << std::endl;

          glDeleteShader(curShader);
        }
        else
        {
          // it compiled, put it in the link list
          shaderNames.push_back(curShader);
        }
      }

      // link the shaders
      this->programName = glCreateProgram();
      for (auto shaderName : shaderNames)
      {
        glAttachShader(this->programName, shaderName);
        glDeleteShader(shaderName);
      }

      glLinkProgram(this->programName);

      glGetProgramiv(this->programName, GL_LINK_STATUS, &compilationStatus);

      if (compilationStatus != GL_TRUE)
      {
        GLint errorLogSize = 0;
        glGetProgramiv(this->programName, GL_INFO_LOG_LENGTH, &errorLogSize);
        std::vector<GLchar> errorLog(errorLogSize);
        glGetProgramInfoLog(this->programName, errorLog.size(), nullptr, errorLog.data());

        std::cerr << "shader program" << std::endl;
        for (const auto& shaderDeets : shaders)
        {
          for (const auto& source : shaderDeets.second)
          {
            std::cerr << '\t' << source.data() << std::endl;
          }
        }
        std::cerr <<
          "failed to link:" << std::endl <<
          errorLog.data() << std::endl;
      }

      // get the uniform names and locations
      GLint activeUniformCount, activeUniformNameLength;
      glGetProgramiv(this->programName, GL_ACTIVE_UNIFORMS, &activeUniformCount);
      glGetProgramiv(this->programName, GL_ACTIVE_UNIFORM_MAX_LENGTH, &activeUniformNameLength);
      std::vector<GLchar> uniformName(activeUniformNameLength);

      for (GLint currentUniform = 0; currentUniform < activeUniformCount; ++currentUniform)
      {
        glGetActiveUniformName(
            this->programName, currentUniform, uniformName.size(), nullptr, 
            uniformName.data()
              );

        this->uniformMap.insert(std::make_pair(
              uniformName.data(),
              glGetUniformLocation(this->programName, uniformName.data())
                  ));
      }
    }

    Shader::~Shader()
    {
      glDeleteProgram(this->programName);
    }

    Shader::Shader(Shader&& rhs) :
      programName(rhs.programName),
      uniformMap(std::move(rhs.uniformMap))
    {
      rhs.programName = 0;
    }

    Shader& Shader::operator=(Shader&& rhs)
    {
      this->programName = rhs.programName;
      this->uniformMap = std::move(rhs.uniformMap);
      rhs.programName = 0;
      return *this;
    }

    void Shader::activate()
    {
      // select the shader
      glUseProgram(this->programName);

      // complete any buffered uploads
      for (auto& bufferedUpload : this->uploadBuffer)
      {
        (*bufferedUpload)();
      }
    }

    GLint Shader::getLocation(const std::string& uniformName)
    {
      auto returnIterator = this->uniformMap.find(uniformName);
      if (returnIterator != this->uniformMap.end())
      {
        return returnIterator->second;
      }
      else
      {
        std::cerr << "trying to get a bad uniform: " << uniformName << std::endl <<
          "existing uniforms:" << std::endl;
        for (const auto& uniform : this->uniformMap)
        {
          std::cerr << uniform.first << std::endl;
        }
        std::cerr << "end uniforms" << std::endl;
        return -1;
      }
    }

    // useful in turning strings into shader source
    // shader source stored this way to allow easy file loading
    std::vector<GLchar> Shader::toCharVector(const std::string& source)
    {
      std::vector<GLchar> out;
      out.reserve(source.size() + 1);

      for (auto character : source)
      {
        out.emplace_back(character);
      }

      out.emplace_back('\0');

      return std::move(out);
    }

    std::vector<GLchar> Shader::terminate(std::vector<GLchar>&& source)
    {
      if (source.back() != '\0')
      {
        source.emplace_back('\0');
      }

      return std::move(source);
    }
  }
}
