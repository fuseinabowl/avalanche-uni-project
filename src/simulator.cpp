#include "simulator.h"
#include "landscape/landscape.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <array>

#include "openGL.h"
#include <SFML/Window.hpp>

#include <csignal>

namespace Avalanche
{
  Simulator::Simulator(const Landscape::Landscape&, const GLuint) noexcept:
    graphics()
    //physics(this->graphics, landscape)
  {
  }

  Simulator::Results Simulator::ProcGenSimulator(unsigned int width, unsigned int height)
  {
    // procedurally generated constructor
    std::vector<float> meshData;
    meshData.reserve(width * height);

    // generate the mesh
    // TODO: put in like Perlin or Simplex or something
    for (unsigned int y = 0; y < height; ++y)
    {
      for (unsigned int x = 0; x < width; ++x)
      {
        meshData.push_back(0.0);
      }
    }

    Landscape::Landscape generatedLandscape(std::move(meshData), width, 1.0, 16,16,16, OpenGL::Texture(std::vector<std::array<GLfloat, 4> >(16), 4));

    Simulator internalSim(generatedLandscape, 0);

    internalSim.run();
  }

  class DebugDrawerConstructorException : public std::exception
  {
    std::string exceptionString;
   public:
    ~DebugDrawerConstructorException() noexcept
    {
    }

    DebugDrawerConstructorException(std::string&& string) noexcept :
      exceptionString(std::move(string))
    {}

    const char* what() const noexcept 
    {
      return this->exceptionString.c_str();
    }
  };

  GLuint loadShader(GLuint shaderType, std::string filename) throw (DebugDrawerConstructorException)
  {
    GLuint shaderName = glCreateShader(shaderType);
    std::fstream shaderSource(filename, std::fstream::in | std::fstream::ate);

    try
    {
      if (shaderSource.good())
      {
        // get the filesize, then seek to the beginning of the file to read it
        int fileSize = shaderSource.tellg();
        shaderSource.seekg(0);

        // try to load the file into a vector (managed array)
        std::vector<GLchar> sourceInMemory(fileSize);

        shaderSource.read(&sourceInMemory.front(), fileSize);

        const GLchar* source(&sourceInMemory.front());
        GLint* sourceLengths(&fileSize);

        glShaderSource(shaderName, 1, &source, sourceLengths);
        glCompileShader(shaderName);

        GLint compilationStatus;
        glGetShaderiv(shaderName, GL_COMPILE_STATUS, &compilationStatus);

        if (GL_TRUE != compilationStatus)
        {
          std::stringstream logStream;
          logStream << "shader \"" << filename << "\" (" << shaderType << ") failed to compile:" << std::endl;

          GLint logLength;
          glGetShaderiv(shaderName, GL_INFO_LOG_LENGTH, &logLength);

          std::vector<GLchar> log(logLength);
          glGetShaderInfoLog(shaderName, logLength, nullptr, &log.front());

          logStream << &log.front() << std::endl;

          throw DebugDrawerConstructorException(logStream.str());
        }
      }
      else
      {
        throw DebugDrawerConstructorException("file inaccessbile");
      }
    }
    catch (DebugDrawerConstructorException& ex)
    {
      glDeleteShader(shaderName);
      throw ex;
    }

    return shaderName;
  }

  class DebugDrawer : public btIDebugDraw
  {
    sf::Window display;
    GLuint shaderProgramName;
    GLuint colourUniform;
    GLuint bufferName;
   public:
    DebugDrawer() throw(DebugDrawerConstructorException):
      // set up window
      display(sf::VideoMode(640, 480, 32), "debug drawer", sf::Style::Default, sf::ContextSettings(12,6,0, 4,2))
    {
      // get all the OpenGL functions ready
      GLint glewInitCode = glewInit();
      if (GLEW_OK != glewInitCode)
      {
        std::stringstream exceptionStream;
        exceptionStream << "glew failed, error code " << glewInitCode << std::endl;
        throw DebugDrawerConstructorException(exceptionStream.str());
      }

      glEnable(GL_DEPTH_TEST);
      glClearColor(1.f,1.f,1.f,1.f);

      // set up shaders

      // make the vertex shader
      GLuint vertexShader = loadShader(GL_VERTEX_SHADER, "src/shaders/debug.vert");

      // make the fragment shader
      GLuint fragmentShader = loadShader(GL_FRAGMENT_SHADER, "src/shaders/debug.frag");

      // link the shader program
      this->shaderProgramName = glCreateProgram();
      glAttachShader(this->shaderProgramName, vertexShader);
      glAttachShader(this->shaderProgramName, fragmentShader);
      glLinkProgram(this->shaderProgramName);
      GLint linkStatus = 0;
      glGetProgramiv(this->shaderProgramName, GL_LINK_STATUS, &linkStatus);
      if (GL_TRUE != linkStatus)
      {
        std::stringstream exceptionStream;
        exceptionStream << "error linking debug shader" << std::endl;

        GLint logLength;
        glGetProgramiv(this->shaderProgramName, GL_INFO_LOG_LENGTH, &logLength);

        std::vector<GLchar> logString(logLength);
        glGetProgramInfoLog(this->shaderProgramName, logLength, nullptr, &logString.front());

        exceptionStream << &logString.front() << std::endl;

        // clean up
        glDeleteProgram(this->shaderProgramName);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        // throw an exception
        throw DebugDrawerConstructorException(exceptionStream.str());
      }

      // flag the shaders for deletion
      glDeleteShader(vertexShader);
      glDeleteShader(fragmentShader);

      // populate colourUniform with the uniform location

      // shader stuff is finished

      // set up array buffer
      glGenBuffers(1, &this->bufferName);
      glBindBuffer(GL_ARRAY_BUFFER, this->bufferName);
      glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), nullptr, GL_STREAM_DRAW);
    }

    ~DebugDrawer() noexcept
    {
      // destroy the shader program
      glDeleteProgram(this->shaderProgramName);
    }

    void drawLine(const btVector3& from, const btVector3& to, const btVector3& colour)
    {
      std::cout << "drawing line" << std::endl;
      std::array<GLfloat, 8> values{{from.x(), from.y(), from.z(), from.w(), to.x(), to.y(), to.z(), to.w()}};

      glUniform4f(this->colourUniform, colour.x(), colour.y(), colour.z(), colour.w());
      glBufferSubData(GL_ARRAY_BUFFER, 0, 8 * sizeof(GLfloat), &values[0]);
      glDrawArrays(GL_LINES, 0, 2);
    }

    // null implementations
    void drawContactPoint(const btVector3&, const btVector3&, btScalar, int, const btVector3&)
    {}

    void reportErrorWarning(const char* errorString)
    {
      std::cout << "bullet warning: " << errorString << std::endl;
    }

    void draw3dText(const btVector3&, const char*)
    {}

    void setDebugMode(int)
    {}

    int getDebugMode() const
    {
      return 0;
    }

    bool finish() // returns false if the window has been closed
    {
      display.display(); // swaps the buffers
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // check to see if the user wants to close the window
      sf::Event event;
      while(display.pollEvent(event))
      {
        if (event.type == sf::Event::Closed)
        {
          return false;
        }
      }

      return true;
    }
  };

  Simulator::Results Simulator::run() noexcept
  {
    std::cout << "running simulator" << std::endl;

    try
    {
      DebugDrawer myDrawer;

      //this->physics.setDebugDrawer(myDrawer);

      sf::Clock timer;
      float time = timer.getElapsedTime().asSeconds();
      float timeStep = 0;
      while (myDrawer.finish())
      {
        timeStep = timer.getElapsedTime().asSeconds() - time;
        time += timeStep;
        //this->physics.step(timeStep);
        //this->physics.debugDraw();
      }
    }
    catch (DebugDrawerConstructorException& ex)
    {
      std::cout << ex.what() << std::endl;
    }
  }
}
