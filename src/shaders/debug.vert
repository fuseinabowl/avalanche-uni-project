#version 330

layout(location = 0) in vec4 location;

void main()
{
  gl_Position = location;
}
