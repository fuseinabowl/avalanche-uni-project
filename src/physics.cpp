#include "physics.h"
#include "landscape/landscape.h"

namespace Avalanche
{
  PhysicsEngine::PhysicsEngine(GraphicsEngine& graphicsRef, const Landscape::Landscape& landscape) :
    // world stuff
    graphics(&graphicsRef),
    collisionConfiguration(),
    collisionDispatcher(&this->collisionConfiguration),
    broadphase(),
    constraintSolver(),
    world(&this->collisionDispatcher, &this->broadphase, &this->constraintSolver, &this->collisionConfiguration),

    // landscape stuff
    landscapeMesh(landscape.getMesh()),
    landscapeShape(this->landscapeMesh.get(), true), // TODO find out what quantisedAabbCompression does (the boolean value)
    landscapeMotionState(), // fill this constructor to alter where the landscape will be in the world
    landscapeBody(btRigidBody::btRigidBodyConstructionInfo(
          0, // mass. 0 = immoveable
          &this->landscapeMotionState, // change the earlier constructor to change the location of the body
          &this->landscapeShape // the shape used by this object, as described earlier
          // no inertia on the landscape
            )),
    // no need to initialise the core bodies and auxiliaries to the core (yet?)
    debugDrawer(nullptr)
  {
    // add the landscape body to the world so it can interact with the core
    this->world.addRigidBody(&this->landscapeBody);
  }

  void PhysicsEngine::step(float timeStep)
  {
    // do physics step
    this->world.stepSimulation(timeStep, 2, 0.1);
    this->world.debugDrawWorld();
  }

  void PhysicsEngine::setDebugDrawer(btIDebugDraw& debugDrawerRef)
  {
    this->debugDrawer = &debugDrawerRef;
    this->world.setDebugDrawer(this->debugDrawer);
  }

  void PhysicsEngine::debugDraw()
  {
    this->world.debugDrawWorld();
  }
}
