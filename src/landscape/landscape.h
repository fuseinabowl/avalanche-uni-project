#ifndef _LANDSCAPE_H_
#define _LANDSCAPE_H_

#include <vector>
#include <array>
#include <stdexcept>
#include "openGL/texture.h"
#include "openGL/camera.h"
#include "openGL/shader.h"
#include "openGL/VBO.h"

namespace Avalanche
{
  namespace Landscape
  {
    class Landscape
    {
      OpenGL::VolumeTexture noslipTex;
      OpenGL::Texture landscapeTexture;
      //OpenGL::NormalMap normalMap;
      OpenGL::TriangleStrip landscapeMesh;

      mutable OpenGL::Shader landscapeShader;
      GLint 
        modelViewLocation,
        projectionLocation,
        normalLocation;

      static OpenGL::TriangleStrip constructTriangleStrip(const std::vector<GLfloat>& heightmap, unsigned heightmapWidth);
     public:
      Landscape(const std::vector<GLfloat>& heightmap, unsigned heightmapWidth, GLfloat landscapeMaxHeight, unsigned int width, unsigned int height, unsigned int depth, OpenGL::Texture&& landscapeTexture) throw (std::logic_error);
      ~Landscape();

      const OpenGL::VolumeTexture& getNoslip() const;

      // assumes you've set up the context for rendering
      void render(const OpenGL::Camera&) const;

      // binds the normal map to the current texture unit
      void bindNormalMap();
    };
  }
}

#endif
