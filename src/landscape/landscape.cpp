#include "landscape.h"
#include <algorithm>
#include "loadfile.h"
#include <iostream>

namespace Avalanche
{
  namespace Landscape
  {
    class HeightGetter
    {
      const std::vector<GLfloat>* heightmap;
      unsigned int width, height;
     public:
      HeightGetter(const std::vector<GLfloat>& heightmap, unsigned width, unsigned height) :
        heightmap(&heightmap),
        width(width), height(height)
      {
      }

      GLfloat getHeight(int x, int y)
      {
        // clamp the get values so they're within the grid
        x = std::min((int)width - 1, std::max(0, x));
        y = std::min((int)height - 1, std::max(0, y));

        return heightmap->at(x + width * y);
      }

    };

    OpenGL::TriangleStrip Landscape::constructTriangleStrip(const std::vector<GLfloat>& heightmap, unsigned width)
    {
      unsigned height = heightmap.size() / width;
      // construct a mesh
      std::deque<std::array<GLfloat, 8> > meshData(width * height);
      HeightGetter getter(heightmap, width, height);

      for (int x = 0; x < (int)width; ++x)
      {
        for (int y = 0; y < (int)height; ++y)
        {
          auto& currentVertex(meshData.at(x + y * width));

          // position
          currentVertex[0] = x;
          currentVertex[1] = getter.getHeight(x,y);
          currentVertex[2] = y;

          // normal
          // calculate the x and y gradients, use z = 1.0, then normalise all three
          float theta = std::atan(0.5 * (getter.getHeight(x + 1,y) - getter.getHeight(x - 1, y)));
          currentVertex[3] = std::sin(theta);
          currentVertex[5] = currentVertex[4] = std::cos(theta);
           
          theta = std::atan(0.5 * (getter.getHeight(x, y + 1) - getter.getHeight(x, y - 1)));
          currentVertex[5] = std::sin(theta);
          float cosTheta = std::cos(theta);
          currentVertex[4] *= cosTheta;
          currentVertex[3] *= cosTheta;

          GLfloat normalisationCoefficient = 0.0;

          for (unsigned char currentCoefficient = 3; currentCoefficient < 6; ++currentCoefficient)
          {
            normalisationCoefficient += currentVertex[currentCoefficient] * currentVertex[currentCoefficient];
          }

          normalisationCoefficient = 1.0 / std::sqrt(normalisationCoefficient);

          for (unsigned char currentCoefficient = 3; currentCoefficient < 6; ++currentCoefficient)
          {
            currentVertex[currentCoefficient] *= normalisationCoefficient;
          }
        }
      }

      return OpenGL::TriangleStrip(meshData, width);
    }

    Landscape::Landscape(const std::vector<GLfloat>& heightmap, unsigned heightmapWidth, GLfloat, unsigned int width, unsigned int height, unsigned int depth, OpenGL::Texture&& landscapeTexture) throw (std::logic_error):
      noslipTex(width, height, depth),
      landscapeTexture(std::move(landscapeTexture)),
      landscapeMesh(Landscape::constructTriangleStrip(heightmap, heightmapWidth))
    {
      // get the heightmap's width
      unsigned heightmapHeight = heightmap.size() / heightmapWidth;
      if (heightmap.size() != heightmapWidth * heightmapHeight)
      {
        throw std::logic_error("heightmap not rectangular");
      }

      // TODO
      // create the noslip texture
      // it is just noslip alpha values, velocity should be 0 all over - 1.0 where the landscape is and 0.0 where the landscape isn't
      // for each (x,y), determine how much of the z should be filled directly from
      // the heightmap, then upload that column of data

      // create the rendering shader
      this->landscapeShader = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/landscape/shaders/basic.vert"))}},
          {GL_FRAGMENT_SHADER, {OpenGL::Shader::terminate(loadFile("src/landscape/shaders/basic.frag"))}}
            });
      this->landscapeShader.activate();
      glUniform1i(this->landscapeShader.getLocation("satelliteImage"), 0);

      this->modelViewLocation = this->landscapeShader.getLocation("modelView");
      this->projectionLocation = this->landscapeShader.getLocation("projection");
      this->normalLocation = this->landscapeShader.getLocation("normalTransform");
    }

    Landscape::~Landscape()
    {}

    void Landscape::render(const OpenGL::Camera& camera) const
    {
      this->landscapeShader.activate();

      // upload the camera details to the shader
      camera.uploadModelView(this->modelViewLocation);
      camera.uploadProjection(this->projectionLocation);
      camera.uploadNormal(this->normalLocation);
      // make sure reset is enabled and at the right value
      this->landscapeMesh.setUp();

      // bind the satellite image
      glActiveTexture(GL_TEXTURE0);
      this->landscapeTexture.bind();

      // render the mesh
      this->landscapeMesh.draw();

      this->landscapeMesh.tearDown();
    }
  }
}
