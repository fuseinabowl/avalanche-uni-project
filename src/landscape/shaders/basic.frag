#version 330

in vec2 textureCoordinates;
in vec3 fragNormal;

uniform sampler2D satelliteImage;

const vec3 lightVector = vec3(sqrt(2.0), sqrt(2.0), 0.0);

layout(location = 0) out vec4 color;

void main()
{
  // sample the texture
  vec4 texSample = texture(satelliteImage, textureCoordinates);

  // work out the diffuse lighting term
  float lightFactor = dot(fragNormal, lightVector);

  color = lightFactor * texSample;
  //color = texSample + 0.1;
  color = vec4(lightFactor) * vec4(textureCoordinates.xy / 8.0, 1.0, 1.0);
}
