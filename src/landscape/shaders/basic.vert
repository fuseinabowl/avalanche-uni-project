#version 330

uniform mat4 projection;
uniform mat4 modelView;
uniform mat3 normalTransform;

layout(location = 0) in vec3 modelSpacePosition;
layout(location = 1) in vec3 normal;

out vec2 textureCoordinates;
out vec3 fragNormal;

void main()
{
  vec4 position = vec4(modelSpacePosition, 1.0) * modelView;
  gl_Position = position * projection; // finished shader / projection matrix test

  // transform the normals
  fragNormal = normal;// * normalTransform;

  // texture position is just the first two coords of the modelSpacePosition
  // i.e. the texture is blanketed onto the landscape
  textureCoordinates = modelSpacePosition.xz;
}
