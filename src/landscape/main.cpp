#include "landscape.h"
#include "openGL/texture.h"
#include <SFML/Window.hpp>
#include <iostream>
#include <GL/glew.h>
#include <cmath>

using namespace Avalanche;

int main()
{
  const float
    width = 4.0,
    height = 4.0;
  const float
    appWidth = 640.0,
    appHeight = 480.0;

  std::vector<GLfloat>heights(width * height);
  for (unsigned x = 0; x < width; ++x)
  {
    for (unsigned y = 0; y < height; ++y)
    {
      heights.at(x + width * y) = 0.1 * x * y;
    }
  }

  sf::Window app(sf::VideoMode(appWidth, appHeight, 32), "landscape test", sf::Style::Default, sf::ContextSettings(16,0,0, 3,3));
  app.setActive(true);

  glewInit();

  glEnable(GL_DEPTH_TEST);

  OpenGL::Texture tex(std::vector<std::array<GLfloat, 4> >(4, {{0.5, 0.1, 0.1, 1.0}}), 2);
  glActiveTexture(GL_TEXTURE0);
  tex.bind();

  Landscape::Landscape land(
      heights, width,
      1,
      2,2,2,
      std::move(tex)
        );

  OpenGL::Camera cam;

  glViewport(0,0, appWidth,appHeight);
  glClearColor(0.0,0.0,0.0,0.0);

  sf::Event event;
  bool close = false;
  float yRot = 0.0, xRot = 0.0;
  float zoom = 0.1;
  cam.frustum(zoom,zoom, 1.0, 100.0);
  while(false == close)
  {
    // allow the demo to close
    while(app.pollEvent(event))
    {
      if (sf::Event::Closed == event.type)
      {
        close = true;
        break;
      }
      else if (sf::Event::Resized == event.type)
      {
        auto& dimensions = event.size;
        glViewport(0,0, dimensions.width, dimensions.height);
      }
      else if (sf::Event::MouseMoved == event.type)
      {
        xRot = 2.0f * M_PI / app.getSize().x * event.mouseMove.x;
        yRot = 2.0f * M_PI / app.getSize().y * (app.getSize().y - event.mouseMove.y);
      }
      else if (sf::Event::MouseWheelMoved == event.type)
      {
        zoom += 0.1 * event.mouseWheel.delta;
        cam.frustum(zoom,zoom, 1.0, 100.0);
      }
    }

    // do another frame
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float cosXRot = std::cos(xRot);
    float sinXRot = std::sin(xRot);

    float cosYRot = std::cos(yRot);
    float sinYRot = std::sin(yRot);

    static const float positionDist(5.0);
    cam.position(
        {{cosYRot * sinXRot * positionDist + (width - 1) / 2.0f, positionDist * sinYRot, cosYRot * cosXRot * positionDist + (height - 1) / 2.0f}},
        {{-cosYRot * sinXRot, -sinYRot, -cosYRot * cosXRot}}, 
        {{ sinYRot * sinXRot, -cosYRot,  sinYRot * cosXRot}}); 

    land.render(cam);
    app.display();
  }
}
