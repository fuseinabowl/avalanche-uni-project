#include "density_advector.h"
#include "loadfile.h"
#include <random>
#include <chrono>
#include <iostream>

namespace Avalanche
{
  namespace NS
  {
    // blank overlay texture
    // the first call to this must be done inside a sf::Context, otherwise the
    // texture constructor will screw up. Symptoms of this are segfaults
    const OpenGL::VolumeTexture& DensityAdvector::getBlankOverlay()
    {
      std::vector<std::array<GLfloat, 4> > blankOverlayData(1, {{0.0}});
      static const OpenGL::VolumeTexture blankOverlay(blankOverlayData, 1, 1);

      return blankOverlay;
    }

    DensityAdvector::DensityAdvector(const Solver& _solver) :
      solver(&_solver), context(),
      textureIndex(0),
      overlayTextureToBeUsed(&DensityAdvector::getBlankOverlay())
    {
      // explicitly set the context active
      this->context.setActive(true);

      // initialise the textures
      auto dimensions = this->solver->getSize();
      unsigned 
        width = dimensions[0],
        height = dimensions[1];
      std::vector<std::array<GLfloat, 4> > textureDefaultData(width * height * dimensions[2], {{0.0,0.0,0.0,0.0}});

      // test data
      std::default_random_engine gen(0);//std::chrono::system_clock::now().time_since_epoch().count());
      std::uniform_real_distribution<float> dist(0,0.5);

      // initialise the red in each texel to a random value
      for (auto& cell : textureDefaultData)
      {
        cell[0] += std::pow(dist(gen), 3.0);
      }

      for (auto& currentTex : this->densityTextures)
      {
        currentTex = OpenGL::VolumeTexture(textureDefaultData, width, height);
      }

      // initialise the framebuffer object
      glGenFramebuffers(1, &this->framebufferName);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->framebufferName);

      // initialise the shaders
      // the shaders will remain in use for the whole time this context is active,
      // so no need to do glUseProgram in the operator()
      std::stringstream resolutionConstString;
      resolutionConstString << std::endl <<
        "const vec3 resolution = vec3(" << dimensions[0] << ".0, " << dimensions[1] << ".0, " << dimensions[2] << ".0);" << std::endl <<
        "const float timestep = " << this->solver->getTimestep() << ";" << std::endl <<
        "const vec3 worldSpaceToTextureSpace = vec3(" << 
          1.0f / (dimensions[0]) << ", " <<
          1.0f / (dimensions[1]) << ", " <<
          1.0f / (dimensions[2]) << ");" << std::endl;
      this->advectorAndOverlay = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/advection.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/density_advector.glsl"))
              }}
                });
      // set up the samplers
      this->advectorAndOverlay.activate();
      glUniform1i(this->advectorAndOverlay.getLocation("velocity"), 0);
      glUniform1i(this->advectorAndOverlay.getLocation("density"), 1);
      glUniform1i(this->advectorAndOverlay.getLocation("overlayDensity"), 2);
      // remember the layer uniform location for use in the operator()
      this->layerUniformLocation = this->advectorAndOverlay.getLocation("layer");

      // initialise the sync object
      this->syncObject = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);

      // set up the viewport
      glViewport(0,0, width, height);

      // deactivate the context so other people can't mess with it
      this->context.setActive(false);
    }

    DensityAdvector::~DensityAdvector()
    {
      this->context.setActive(true);

      glDeleteFramebuffers(1, &this->framebufferName);
      glDeleteSync(this->syncObject);
      // keep context active so destructors work properly
      this->context.setActive(false);
    }

    void DensityAdvector::operator()()
    {
      // set up the context
      this->context.setActive(true);
      this->unitSquare.setUp();

      OpenGL::VolumeTexture
        & sourceTexture(this->densityTextures[this->textureIndex]),
        & renderToTexture(this->densityTextures[(this->textureIndex + 1) % this->densityTextures.size()]);

      auto texDimensions = renderToTexture.getDimensions();
      glViewport(0,0, texDimensions[0], texDimensions[1]);

      // bind the current velocity texture from the referenced solver to unit 0
      glActiveTexture(GL_TEXTURE0);
      this->solver->getVelocityTexture().bind();

      // bind the last calculated density texture to unit 1
      glActiveTexture(GL_TEXTURE1);
      sourceTexture.bind();

      // if changed, bind the overlay texture to unit 2
      if (nullptr != this->overlayTextureToBeUsed)
      {
        glActiveTexture(GL_TEXTURE2);
        this->overlayTextureToBeUsed->bind();
        // mark the overlay texture as unchanged again
        this->overlayTextureToBeUsed = nullptr;
      }

      // advect the densities to the next densityTexture
      unsigned depth = this->solver->getSize()[2];
      for (GLuint layer = 0; layer < depth; ++layer)
      {
        // set up the draw location
        glFramebufferTexture3D(
            GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D,
            renderToTexture.getName(), 0, layer);

        // upload the depth to the uniform
        glUniform1ui(this->layerUniformLocation, layer);

        // draw for this layer
        this->unitSquare.draw();
      }

      // update the texture index (which textures will be returned from getDensityTextures and which textures will be used as source and destination next iteration)
      // and synchronise the GLsync
      {
        std::unique_lock<std::mutex> syncLock(this->syncAccess);
        glDeleteSync(this->syncObject);
        this->syncObject = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);

        this->textureIndex = (this->textureIndex + 1) % this->densityTextures.size();
      }

      this->unitSquare.tearDown();
      glFlush();
      this->context.setActive(false);
    }

    std::array<const OpenGL::VolumeTexture*, 2> DensityAdvector::getDensityTextures() const
    {
      std::unique_lock<std::mutex> syncLock(this->syncAccess);
      return {{&this->densityTextures[(this->textureIndex - 1 + this->densityTextures.size()) % this->densityTextures.size()], &this->densityTextures[this->textureIndex]}};
    }

    void DensityAdvector::setDensityOverlay(const OpenGL::VolumeTexture& tex)
    {
      this->overlayTextureToBeUsed = &tex;
    }
  }
}
