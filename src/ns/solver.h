#ifndef _NS_SOLVER_H_
#define _NS_SOLVER_H_

#include "openGL.h"
#include <array>
#include <SFML/Window.hpp>
#include "openGL/texture.h"
#include "openGL/shader.h"
#include <mutex>

namespace Avalanche
{
  namespace NS
  {
    class Solver
    {
      public:
      // context so that this doesn't get screwed up by other functions messing around
      // with openGL state
      sf::Context context;

      unsigned width, height, depth;
      unsigned numberOfIterations;

      static const OpenGL::VolumeTexture& getBlankTexture();

      OpenGL::VolumeTexture velocityTexture; 

      // input texture buffers
      const OpenGL::VolumeTexture* velocityOverlayField;

      // solver inner workings
      GLuint framebufferName;
      OpenGL::UnitSquare squareMesh;
      OpenGL::VolumeTexture 
        intermediates[2];
      OpenGL::VolumeTexture
        tempVelocity,
        velocityDivergenceTex;

      OpenGL::Shader velocityAdvector;
      GLint velAdvectLayerLocation;

      OpenGL::Shader velocityDiffuser;
      GLint velDiffuseLayerLocation;

      OpenGL::Shader forceApplier;
      GLint forceApplierLayerLocation;

      OpenGL::Shader velocityDivergence;
      GLint velDivergenceLayerLocation;

      OpenGL::Shader pressureCalculator;
      GLint pressureCalculatorLayerLocation;

      OpenGL::Shader borderControl;
      GLint borderScaleLocation;
      GLint borderLayerLocation;
      GLint borderTexLocation;

      OpenGL::Shader pressureSubtractor;
      GLint pressureSubtractorLayerLocation;

      // private functions to split up the functionality
      void setUpShaders(unsigned numberOfIterations, float viscosity);

      // synchronise with other threads and contexts
      GLsync syncObject;
      mutable std::mutex syncObjectAccess;
     public:
      Solver(unsigned width, unsigned height, unsigned depth, float viscosity);
      ~Solver();

      // use these functions to add stuff to the simulation
      // add objects by applying an overlay texture (uses alpha blending)
      void setVelocityOverlay(const OpenGL::VolumeTexture& textureName);

      void operator()(); // steps the simulation

      // returns the current velocity texture
      // also synchronises you with the simulator thread and context
      const OpenGL::VolumeTexture& getVelocityTexture() const;
      std::array<unsigned, 3> getSize() const;
      GLfloat getTimestep() const;
    };
  }
}

#endif
