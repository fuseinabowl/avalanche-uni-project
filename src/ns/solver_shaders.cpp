#include "solver.h"

#include <vector>
#include <string>
#include <sstream>
#include "loadfile.h"
#include <memory>
#include <iostream>

namespace Avalanche
{
  namespace NS
  {
    void Solver::setUpShaders(unsigned numberOfIterations, float viscosity)
    {

      // select target texture to be 0th level of intermediate tex out
      // set up shader to be advector
      // upload uniform to be in line with the 0th level
      // calculate the time step for advection
      numberOfIterations = numberOfIterations != 0 ? numberOfIterations : 1;
      GLfloat timestep = this->getTimestep();
      this->numberOfIterations = numberOfIterations;

      std::stringstream resolutionConstString;
      resolutionConstString << std::endl <<
        "const vec3 worldSpaceToTextureSpace = vec3(" << 
          1.0f / this->width << ", " <<
          1.0f / this->height << ", " <<
          1.0f / this->depth << ");" << std::endl <<
        "const float timestep = " << timestep << ";" << std::endl <<
        "const float viscosity = " << viscosity << ";" << std::endl <<
        "const ivec3 resolution = ivec3(" <<
          this->width << ", " <<
          this->height << ", " <<
          this->depth << ");" << std::endl;

      // -- VELOCITY ADVECTION --
      this->velocityAdvector = OpenGL::Shader ({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/advection.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/velocity_advect.glsl"))
              }}
                });

      this->velocityAdvector.activate();
      // we are always going to use shader unit 0 as the source
      glUniform1i(this->velocityAdvector.getLocation("velocitySampler"), 0);
      // also store the current layer value
      this->velAdvectLayerLocation = this->velocityAdvector.getLocation("layer");

      // -- VELOCITY DIFFUSE -- 
      // velocity diffusion shader, corresponds to section 
      this->velocityDiffuser = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/jacobi.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/velocity_diffuse.glsl"))
              }}
                });

      this->velocityDiffuser.activate();
      glUniform1i(this->velocityDiffuser.getLocation("velocitySampler"), 0);
      this->velDiffuseLayerLocation = this->velocityDiffuser.getLocation("layer");

      // -- FORCE APPLICATION --
      this->forceApplier = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/force_applier.glsl"))
              }}
                });
      this->forceApplier.activate();
      glUniform1i(this->forceApplier.getLocation("velocityOverlay"), 3);
      glUniform1i(this->forceApplier.getLocation("velocity"), 0);
      this->forceApplierLayerLocation = this->forceApplier.getLocation("layer");

      // -- PROJECT 1 --
      this->velocityDivergence = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/divergence.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/velocity_divergence.glsl"))
              }}
                });
      this->velocityDivergence.activate();
      glUniform1i(this->velocityDivergence.getLocation("velocity"), 1);
      this->velDivergenceLayerLocation = this->velocityDivergence.getLocation("layer");
      glActiveTexture(GL_TEXTURE1);
      this->tempVelocity.bind();
      glActiveTexture(GL_TEXTURE0);

      // -- PROJECT 2 --
      this->pressureCalculator = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/jacobi.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/pressure_calculator.glsl"))
              }}
                });
      this->pressureCalculator.activate();
      glUniform1i(this->pressureCalculator.getLocation("previousPressure"), 0);
      glUniform1i(this->pressureCalculator.getLocation("velocityField"), 2);
      this->pressureCalculatorLayerLocation = this->pressureCalculator.getLocation("layer");
      glActiveTexture(GL_TEXTURE2);
      this->velocityDivergenceTex.bind();
      glActiveTexture(GL_TEXTURE0);

      // -- PROJECT 3 -- 
      this->borderControl = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/border_control.glsl"))
              }}
                });
      this->borderScaleLocation = this->borderControl.getLocation("scale");
      this->borderLayerLocation = this->borderControl.getLocation("layer");
      this->borderTexLocation = this->borderControl.getLocation("inTex");
      // pressure will be stored in unit 0 (currentDraw and currentSource intermediates)
      // temp velocity is already bound to tex unit 1 in PROJECT 2

      // -- PROJECT 4 -- 
      this->pressureSubtractor = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(OpenGL::Shader::toCharVector(resolutionConstString.str())),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/gradient.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/velocity_project.glsl"))
              }}
                });
      this->pressureSubtractor.activate();
      glUniform1i(this->pressureSubtractor.getLocation("sourceVelocity"), 0);
      glUniform1i(this->pressureSubtractor.getLocation("pressure"), 4);
      this->pressureSubtractorLayerLocation = this->pressureSubtractor.getLocation("layer");
    }

    // helper function, swaps over the two pointers and then binds the
    // old output (1st param) as the new input
    void swapAndBind(OpenGL::VolumeTexture*& draw, OpenGL::VolumeTexture*& read)
    {
      std::swap(draw, read);
      glBindTexture(GL_TEXTURE_3D, draw->getName());
    }

    void Solver::operator()()
    {
      this->context.setActive(true);

      // set up the square mesh for drawing
      this->squareMesh.setUp();

      // set up the intermediate textures
      auto
        currentDraw = &this->intermediates[0],
        currentSource = &this->intermediates[1];

      // the input is originally from the current velocity field
      glActiveTexture(GL_TEXTURE0);
      this->velocityTexture.bind();
      // the first output is currentDraw
      // currentSource is unused right now
      // the output needs to be set per layer, so is done in the loop
      // always put the currentSource in texture unit 0

      // -- ADVECTION --

      while(glGetError());
      this->velocityAdvector.activate();

      for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // change the target texture and uniform uploads to the corresponding level
        glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, currentDraw->getName(), 0, currentLayer);
        glUniform1ui(this->velAdvectLayerLocation, currentLayer);

        // draw the buffer
        this->squareMesh.draw();
      }

      // -- DIFFUSE --
      // set up the diffuse step
      this->velocityDiffuser.activate();

      for (unsigned char jacobi = 0; jacobi < 50; ++jacobi)
      {
        // need to swap and bind each jacobi iteration, keep
        // feeding the last iteration into the new iteration
        swapAndBind(currentDraw, currentSource);

        // step through the layers doing the diffuse step
        for (GLuint currentLayer = 1; currentLayer < this->depth; ++currentLayer)
        {
          // set up the layer
          glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, currentDraw->getName(), 0, currentLayer);
          glUniform1ui(this->velDiffuseLayerLocation, currentLayer);

          // draw the buffer
          this->squareMesh.draw();
        }
      }

      // -- FORCES --
      // apply the force field (all cells have mass 1, so the force field is just added on)
      // overlay the no-slip velocity field (use alpha blending to get the forces)
      // step through the forces
      // save this result in the tempVelocity texture
      // render to a temporary texture outside of the intermediates so
      // the intermediates can be used to solve

      swapAndBind(currentDraw, currentSource);
      this->forceApplier.activate();

      // apply any force fields
      if (nullptr != this->velocityOverlayField)
      {
        glActiveTexture(GL_TEXTURE3);
        this->velocityOverlayField->bind();
        this->velocityOverlayField = nullptr;
        glActiveTexture(GL_TEXTURE0);
      }

      for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // set up the layer
        glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, this->tempVelocity.getName(), 0, currentLayer);
        glUniform1ui(this->forceApplierLayerLocation, currentLayer);

        // draw the buffer
        this->squareMesh.draw();
      }

      this->tempVelocity.bind();

      // -- PROJECT 1 - VELOCITY DIVERGENCE --
      this->velocityDivergence.activate();

      for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // set up the layer
        glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, this->velocityDivergenceTex.getName(), 0, currentLayer);
        glUniform1ui(this->velDivergenceLayerLocation, currentLayer);

        // draw the buffer
        this->squareMesh.draw();
      }

      // -- PROJECT 2 - PRESSURE CALCULATION -- 
      // use the jacobi operator to solve the poisson equation of getting a non-divergent velocity field
      // use jacobi to calculate 
      this->pressureCalculator.activate();

      // set the clear colour to zero for our first guess of the pressure jacobi solver
      glClearColor(0.0, 0.0, 0.0, 0.0);

      // for the first round, clear the previous best guess for pressure to 0
      for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // set up the layer
        glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, currentDraw->getName(), 0, currentLayer);

        // clear the layer
        glClear(GL_COLOR_BUFFER_BIT);
      }

      for (unsigned char jacobi = 0; jacobi < 50; ++jacobi)
      {
        // need to swap and bind each jacobi iteration, keep
        // feeding the last iteration into the new iteration
        swapAndBind(currentDraw, currentSource);

        // step through the layers doing the diffuse step
        for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
        {
          // set up the layer
          glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, currentDraw->getName(), 0, currentLayer);
          glUniform1ui(this->pressureCalculatorLayerLocation, currentLayer);

          // draw the buffer
          this->squareMesh.draw();
        }
      }

      // -- PROJECT 3 - BOUNDARY CONDITIONS --
      this->borderControl.activate();

      swapAndBind(currentDraw, currentSource);

      // first do pressure control
      glUniform1f(this->borderScaleLocation, 1.0);
      glUniform1i(this->borderTexLocation, 0); // the last filled pressure calculation

      // step through the layers doing the boundary step
      for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // set up the layer
        glUniform1ui(this->borderLayerLocation, currentLayer);
        glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, currentDraw->getName(), 0, currentLayer);

        // draw the buffer
        this->squareMesh.draw();
      }

      // now do velocity control (back into unused pressure)
      // do not swap and bind, simply render into currentSource
      // so currentDraw = pressure
      //    currentSource = velocity

      glUniform1f(this->borderScaleLocation, -1.0);
      glUniform1i(this->borderTexLocation, 1); // the tempVelocity tex

      // step through the layers doing the boundary step
      for (GLuint currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // set up the layer
        glUniform1ui(this->borderLayerLocation, currentLayer);
        glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, currentSource->getName(), 0, currentLayer);

        // draw the buffer
        this->squareMesh.draw();
      }

      // -- PROJECT 4 - PRESSURE SUBTRACTION -- 
      // output goes into the public velocity field
      // use the tempVelocity as the velocity input and the currentDraw as the pressure input
      // subtract the pressure gradient from the current calculated velocities
      this->pressureSubtractor.activate();

      glActiveTexture(GL_TEXTURE4);
      currentDraw->bind(); // binds pressure to tex unit 4
      glActiveTexture(GL_TEXTURE0);
      currentSource->bind(); // binds velocity to tex unit 0

      for (unsigned currentLayer = 0; currentLayer < this->depth; ++currentLayer)
      {
        // set up the layer
        glFramebufferTexture3D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, this->velocityTexture.getName(), 0, currentLayer);
        glUniform1ui(this->pressureSubtractorLayerLocation, currentLayer);

        // draw the buffer
        this->squareMesh.draw();
      }

      // finally close the square mesh as we don't need it until this is next called
      this->squareMesh.tearDown();

      glFlush();
      this->context.setActive(false);
    }
  }
}
