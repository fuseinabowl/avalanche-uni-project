// gradient function returns the gradient of the indexed color
// gradientx function returns the gradient of the red color, just a shortcut for (..., 0)

vec3 gradient(
    in sampler3D pressure,
    in ivec3 coords,
    const int index
      )
{
  const ivec2 oneZero = ivec2(1,0);

  vec3 sub = vec3(0.0);

  sub.x = 
      texelFetch(pressure, coords + oneZero.xyy, 0)[index]
    - texelFetch(pressure, coords - oneZero.xyy, 0)[index];
  sub.y = 
      texelFetch(pressure, coords + oneZero.yxy, 0)[index]
    - texelFetch(pressure, coords - oneZero.yxy, 0)[index];
  sub.z = 
      texelFetch(pressure, coords + oneZero.yyx, 0)[index]
    - texelFetch(pressure, coords - oneZero.yyx, 0)[index];

  return sub;
}

vec3 gradientx(
    in sampler3D pressure,
    in ivec3 coords
      )
{
  return gradient(pressure, coords, 0);
}
