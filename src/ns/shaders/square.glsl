#version 330

layout(location = 0) in vec2 inPos;
uniform vec4 locationParams;
out vec2 pos;

void main()
{
  gl_Position = vec4(((inPos * locationParams.xy + locationParams.zw) * 2.0 - 1.0), 0.0, 1.0);
  pos = inPos;
}
