float divergence(
    in ivec3 worldCoords,
    in sampler3D vectorField
      )
{
  const ivec2 oneZero = ivec2(1.0, 0.0);
  float sum = 0.0;

  sum += texelFetch(vectorField, worldCoords + oneZero.xyy, 0).x;
  sum -= texelFetch(vectorField, worldCoords - oneZero.xyy, 0).x;

  sum += texelFetch(vectorField, worldCoords + oneZero.yxy, 0).y;
  sum -= texelFetch(vectorField, worldCoords - oneZero.yxy, 0).y;

  sum += texelFetch(vectorField, worldCoords + oneZero.yyx, 0).z;
  sum -= texelFetch(vectorField, worldCoords - oneZero.yyx, 0).z;

  return 0.5 * sum;
}
