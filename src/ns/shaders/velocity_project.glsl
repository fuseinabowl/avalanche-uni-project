layout(location = 0) out vec4 color;

uniform sampler3D sourceVelocity;
uniform sampler3D pressure;
uniform unsigned int layer;

layout(pixel_center_integer) in vec4 gl_FragCoord;

const ivec2 oneZero = ivec2(1, 0);

void main()
{
  ivec3 coord = ivec3(gl_FragCoord.xy, layer);

  color = texelFetch(sourceVelocity, coord, 0) - 
    vec4(0.5 * gradientx(pressure, coord), 0.0);
}
