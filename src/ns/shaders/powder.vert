#version 330

uniform mat4 projection;
//uniform mat4 modelView;

layout(location = 0) in vec3 coords;
layout(location = 1) in vec3 textureCoordsIn;

out vec3 textureCoords;

void main()
{
  //vec4 position = vec4(coords, 1.0) * modelView;
  gl_Position = position * projection;

  textureCoords = textureCoordsIn;
}
