
vec4 advect(
    in vec3 worldPos,
    const float timestep,
    in sampler3D velocity,
    in sampler3D concentration
      )
{
  // get the position where this region would have been one timestep in the past
  vec3 pos = 
    // current position
      worldPos - 
    // offset from previous position, multiplied by the timestep
      timestep * worldSpaceToTextureSpace * texture(velocity, worldPos).xyz;
  // lookup the value at that position
  return texture(concentration, pos);
}
