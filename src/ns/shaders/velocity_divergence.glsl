uniform sampler3D velocity;
uniform unsigned int layer;

layout(location = 0) out vec4 outVelocity;
layout(pixel_center_integer) in vec4 gl_FragCoord;

void main()
{
  ivec3 coords = ivec3(gl_FragCoord.xy, layer);
  outVelocity = 0.5 * worldSpaceToTextureSpace.xyzz * vec4(divergence(coords, velocity));
}
