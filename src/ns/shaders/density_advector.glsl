// requires advection.glsl

uniform sampler3D velocity;
uniform sampler3D density;
uniform sampler3D overlayDensity;
uniform unsigned int layer;

layout(location = 0) out vec4 densities;
layout(pixel_center_integer) in vec4 gl_FragCoord;

void main()
{
  vec3 worldPosition = vec3(gl_FragCoord.xy, layer);
  // need to divide by the resolution and add a half pixel to get it into texture space
  vec3 texturePosition = worldPosition * worldSpaceToTextureSpace + worldSpaceToTextureSpace * 0.5;

  vec4 overlayDensitySample = texture(overlayDensity, texturePosition);
  vec4 advectedDensity = advect(texturePosition, timestep, velocity, density);

  densities = mix(
      // calculated values
      advectedDensity,
      // overlay values
      overlayDensitySample,
      // overlay alpha
      overlayDensitySample.a
        );
  // blending is not disabled, just write at max alpha
  densities.a = 1.0;
}
