uniform sampler3D velocitySampler;
uniform unsigned int layer;

layout(location = 0) out vec4 velocity;
layout(pixel_center_integer) in vec4 gl_FragCoord;

const vec4 alpha = 
  vec4(worldSpaceToTextureSpace * worldSpaceToTextureSpace * timestep / viscosity, 1.0);
  //vec4(vec3(dot(worldSpaceToTextureSpace, worldSpaceToTextureSpace) * timestep / viscosity), 1.0);
  //vec4(vec3(1.0) / viscosity, 1.0);

void main()
{
  ivec3 texCoords = ivec3(gl_FragCoord.xy, layer);

  velocity = jacobi(texCoords, alpha, vec4(1.0) / (alpha + 6.0), velocitySampler, velocitySampler);
}
