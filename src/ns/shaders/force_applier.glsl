uniform sampler3D velocityOverlay;
uniform sampler3D velocity;
uniform unsigned int layer;

layout(location = 0) out vec4 velocityOut;
layout(pixel_center_integer) in vec4 gl_FragCoord;

void main()
{
  // emulates alpha blending
  // src : GL_ONE
  // dest: GL_ONE_MINUS_SRC_ALPHA
  // this adds the force to the velocity for 0 alpha (force application)
  // and replaces the velocity with the force for 1 alpha (no slip application)

  ivec3 texCoord = ivec3(gl_FragCoord.xy, layer);
  vec4 overlaySample = texelFetch(velocityOverlay, texCoord, 0);

  velocityOut = 
      overlaySample + 
      texelFetch(velocity, texCoord, 0) * (1.0 - overlaySample.a);
}
