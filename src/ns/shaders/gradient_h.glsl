vec4 gradient(
    in vec3 worldCoords,
    const vec3 resolution,
    in sampler3D pressure,
    in sampler3D velocity
      );
