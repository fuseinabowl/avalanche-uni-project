float divergence(
    in vec3 coords,
    const vec3 resolution,
    in sampler3D vectorField
      );
