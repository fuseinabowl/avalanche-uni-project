vec4 jacobi(
    in vec3 worldCoords,
    const vec3 resolution,
    in float alpha,
    in float reciprocalBeta,
    in sampler3D x,
    in sampler3D b
      );
