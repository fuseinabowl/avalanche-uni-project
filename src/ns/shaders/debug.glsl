uniform sampler3D values;
uniform int channel = 0;

layout(location = 0) out vec4 color;

in vec2 worldLocation;

const vec4 signColor = vec4(1.0, 0.0, -1.0, 0.0);
const vec4 crazyColor = vec4(0.0, 0.2,  0.0, 0.0);

void main()
{
  float sampleValue = texture(values, vec3(worldLocation, 0.5))[channel];
  sampleValue *= 10.0;

  color = signColor * (0.5 * sign(sampleValue) + sampleValue) + crazyColor * pow(sampleValue, 2.0);
  color.a = 1.0;
}
