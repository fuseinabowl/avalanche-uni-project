in vec3 textureCoords;

layout(location = 0) out vec4 color;

uniform sampler3D powderDensitiesBegin;
uniform sampler3D powderDensitiesEnd;
uniform float interpolation;

uniform vec4 powderColor;

void main()
{
  float sample = mix(
      texture(powderDensitiesBegin, textureCoords),
      texture(powderDensitiesEnd, textureCoords),
      interpolation).r;

  color = powderColor * vec4(1.0,1.0,1.0, sample);
}
