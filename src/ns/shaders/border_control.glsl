uniform sampler3D inTex;
uniform float scale;
uniform unsigned int layer;

layout(location = 0) out vec4 outTexel;
layout(pixel_center_integer) in vec4 gl_FragCoord;

void main()
{
  // find the clamped coords
  ivec3 origCoords = ivec3(gl_FragCoord.xy, layer);
  // use (resolution - 2) because (resolution - 1) = the outermost pixel (the border pixel we are trying to control)
  ivec3 coords = clamp(origCoords, ivec3(1), resolution - ivec3(2));

  // get the texel
  vec4 outTexelTemp = texelFetch(inTex, coords, 0);
  // scale it by scale * (coords not equal to input coords) and then return it
  float appliedScale = mix(1.0, scale, min(1.0, dot(vec3(1.0), step(1.0, vec3(abs(coords - origCoords))))));
  outTexel = vec4(outTexelTemp.xyz * appliedScale, 1.0);
}
