// jacobi function iteratively solves a poisson equations
// x should be the previous iteration's field

vec4 jacobi(
    in ivec3 texCoords,
    in vec4 alpha,
    in vec4 reciprocalBeta,
    in sampler3D x, // should be the result of the previous jacobi iteration
    in sampler3D b
      )
{
  // reimplemented to exactly match the tutorial

  // samples
  // left right
  vec4 xL = texelFetch(x, texCoords - ivec3(1, 0, 0), 0);
  vec4 xR = texelFetch(x, texCoords + ivec3(1, 0, 0), 0);

  // top bottom
  vec4 xT = texelFetch(x, texCoords - ivec3(0, 1, 0), 0);
  vec4 xB = texelFetch(x, texCoords + ivec3(0, 1, 0), 0);

  // front back
  vec4 xF = texelFetch(x, texCoords - ivec3(0, 0, 1), 0);
  vec4 xBa= texelFetch(x, texCoords + ivec3(0, 0, 1), 0);

  // b sample from center
  vec4 bC = texelFetch(b, texCoords, 0);

  // evaluate jacobi iteration
  return (xL + xR + xT + xB + xF + xBa + alpha * bC) * reciprocalBeta;
  //return (xL + xR + xT + xB + alpha * bC) * reciprocalBeta;
}
