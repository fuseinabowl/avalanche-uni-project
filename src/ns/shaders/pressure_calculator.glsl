uniform sampler3D previousPressure;
uniform sampler3D velocityField;
uniform unsigned int layer;

layout(location = 0) out vec4 pressure;
layout(pixel_center_integer) in vec4 gl_FragCoord;

const vec4 alpha = 
  vec4(-resolution * resolution, 1.0);
  //vec4(vec3(- dot(vec3(resolution), vec3(resolution))), 1.0);
  //- vec4(1.0);

void main()
{
  ivec3 worldLoc = ivec3(gl_FragCoord.xy, layer);
  pressure = jacobi(worldLoc, alpha, vec4(1.0 / 6.0), previousPressure, velocityField);
}
