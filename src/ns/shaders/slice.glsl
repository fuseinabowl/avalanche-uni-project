uniform sampler3D fromDensity;
uniform sampler3D toDensity;
uniform float interpolation;
uniform float renderDepth;

layout(location = 0) out vec4 color;

in vec2 worldLocation;

const vec3 smokeColor = vec3(0.7,0.7,1.0);

void main()
{
  // sample at the renderDepth through the density tex
  vec3 worldLoc = vec3(worldLocation, renderDepth);

  vec4 sampleValue = mix(
      texture(fromDensity, worldLoc),
      texture(toDensity, worldLoc),
      interpolation
        );

  // now you've got a density, use the red value as the alpha
  color = vec4(smokeColor, sampleValue.r);
}
