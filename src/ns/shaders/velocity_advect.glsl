
layout(location = 0) out vec4 outVelocity;

// don't require the sampler to be a multisampler,
// instead use two samplers
uniform sampler3D velocitySampler;
uniform unsigned int layer;

in vec4 gl_FragCoord;

void main()
{
  vec3 worldPosition = vec3(gl_FragCoord.xy, layer);
  // need to divide by the resolution and add a half pixel to get it into texture space
  vec3 texturePosition = worldPosition * worldSpaceToTextureSpace;

  outVelocity = advect(
      texturePosition,
      timestep,
      velocitySampler,
      velocitySampler
        );

  outVelocity.a = 1.0;
}
