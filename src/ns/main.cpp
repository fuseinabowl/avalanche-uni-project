// secondary main file, used in testing and demoing the navier stokes solver

#include "demo.h"

#include <iostream>

int main(int argc, const char** argv)
{
  unsigned 
    width  = 256, 
    height = 256, 
    depth  = 4;

  char continueFlag = 1;
  while (continueFlag)
  {
    Avalanche::NS::Demo myDemo(width, height, depth, argc, argv);
    continueFlag = myDemo();
  }

  return 0;
}
