#ifndef _DENSITY_ADVECTOR_H_
#define _DENSITY_ADVECTOR_H_

#include "solver.h"
#include "openGL.h"
#include "openGL/texture.h"
#include "openGL/shader.h"
#include <SFML/Window.hpp>

namespace Avalanche
{
  namespace NS
  {
    class DensityAdvector
    {
      // get the velocity field from the solver every iteration
      const Solver* solver;

      sf::Context context; // initialise after the solver as the solver will disable the context if in the other order

      // density textures
      // superfluous textures allow interpolation between iterations and buffering
      // before context because default constructor doesn't make a texture
      // and needs to be destructed before the 
      std::array<OpenGL::VolumeTexture, 3> densityTextures;
      unsigned char textureIndex;

      // synchronisation items
      mutable std::mutex syncAccess;
      GLsync syncObject;

      // framebuffer
      GLuint framebufferName;

      // density overlay texture
      // once it has been uploaded, it is never used again and the pointer is nullified
      const OpenGL::VolumeTexture* overlayTextureToBeUsed;
      static const OpenGL::VolumeTexture& getBlankOverlay();

      // shaders used in the operator() method
      OpenGL::Shader advectorAndOverlay;
      GLint layerUniformLocation;

      // mesh used for drawing
      OpenGL::UnitSquare unitSquare;
     public:
      DensityAdvector(const Solver& solver);
      ~DensityAdvector();

      void operator()(); // step the advection simulation, does NOT step the associated solver

      std::array<const OpenGL::VolumeTexture*, 2> getDensityTextures() const;
      // overlays this texture every frame 
      void setDensityOverlay(const OpenGL::VolumeTexture& densityTexture);
    };
  }
}

#endif
