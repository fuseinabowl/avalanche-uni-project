#ifndef _POWDER_AVALANCHE_H_
#define _POWDER_AVALANCHE_H_

#include "density_advector.h"
#include "solver.h"
#include "landscape/landscape.h"

#include <SFML/Window.hpp>

namespace Avalanche
{
  namespace NS
  {
    class PowderSimulator
    {
      Solver solver;
      DensityAdvector powder;

      const Landscape::Landscape* landscape; // to get the noSlip texture

      sf::Context context; // for dealing with force workings

      OpenGL::VolumeTexture forceField;
      mutable OpenGL::Shader forceConstructor;
      GLint forceConstructorLayerLocation;

      mutable OpenGL::Shader renderer;
      GLint rendererLayer;
      GLint rendererInterpolation;
      GLint rendererModelView, rendererProjection, rendererNormal;
     public:
      PowderSimulator(const Landscape::Landscape&,
          unsigned width, unsigned height, unsigned depth,
          float airViscosity);

      // step through the simulation
      void operator()();

      void render(const OpenGL::Camera&, GLfloat interpolation) const;
    };
  }
}

#endif
