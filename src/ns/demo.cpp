#include "demo.h"

#include <vector>
#include <chrono>
#include <SFML/Window.hpp>
#include <iostream>
#include <exception>
#include <sstream>
#include "loadfile.h"
#include <memory>
#include <algorithm>

namespace Avalanche
{
  namespace NS
  {
    Demo::GlewInitialiser::GlewFailure::GlewFailure(GLuint statusCode)
    {
      std::stringstream errorStream;
      errorStream << "GLEW failed to initialise, error code was " << statusCode;
      this->failureString = errorStream.str();
    }

    Demo::GlewInitialiser::GlewFailure::~GlewFailure() noexcept
    {
    }

    const char* Demo::GlewInitialiser::GlewFailure::what() noexcept
    {
      return this->failureString.c_str();
    }

    Demo::GlewInitialiser::GlewInitialiser()
    {
      GLenum status = glewInit();
      if (status != GLEW_OK)
      {
        throw GlewFailure(status);
      }
    }

    float getViscosity(int argc, const char** argv)
    {
      if (argc > 1)
      {
        std::stringstream viscosityStream(argv[1]);
        float viscosity;
        viscosityStream >> viscosity;
        return viscosity;
      }
      else
      {
        return 1.0;
      }
    }

    void putBox(std::vector<std::array<GLfloat, 4> >& source, unsigned sourceWidth,
        unsigned xStart, unsigned width, 
        unsigned yStart, unsigned height)
    {
      for (unsigned x = 0; x < width; ++x)
      {
        for (unsigned y = 0; y < height; ++y)
        {
          auto& cell(source[xStart + x + (yStart + y) * sourceWidth]);
          cell[0] = cell[1] = cell[2] = 0.0;
          cell[3] = 1.0;
        }
      }
    }

    Demo::Demo(unsigned width, unsigned height, unsigned depth, int argc, const char** argv) :
      mainScreen(
          sf::VideoMode(width,height),
          "NS solver demo",
          sf::Style::Default,
            // 32 bit colour
            // no depth buffer (we will get our own one), no stencil bits, 
            // no antialiasing (doesn't work on linux anyway),
            // openGL 3.3 at least
          sf::ContextSettings(32, 0, 0, 3,3) 
            ),
      solver(width, height, depth, getViscosity(argc,argv)),
      density(solver),
      sliceWidth(1.0f / depth)
    {
      // reactivate this openGL context
      this->mainScreen.setActive(true);

      // set up the blending
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      glClearColor(0.0,0.0,0.0,1.0);

      // create the source map (starts with completely 0)
      std::vector<std::array<GLfloat, 4> > source(width * height * depth, {{0.0,0.0,0.0,0.0}});

      //putBox(source, width, 20, 60, 80, 20);

      // make a texture and upload the image data to the GPU (use texture unit 0 for this)
      glActiveTexture(GL_TEXTURE0 + 2);
      this->densityOverlay = OpenGL::VolumeTexture(source, width, height);

      glActiveTexture(GL_TEXTURE0);

      // set the velocity source
      std::vector<std::array<GLfloat, 4> > velocitySource(width * height * depth, {{0.0,0.0,0.0,0.0}});
      // create a force field
      for (unsigned x = 0; x < width; ++x)
      {
        for (unsigned y = 0; y < height; ++y)
        {
          for (unsigned z = 0; z < depth; ++z)
          {
            velocitySource[x + y * width + z * width * height][0] = 0.000000001 * 
              std::max(int((-20 + y) * (-20 + width - y)), 0) *
              x * (height - x);
          }
        }
      }

      // create an impassable box in the velocity
      //putBox(velocitySource, width, 20, 60, 80, 20);

      this->velocityOverlay = OpenGL::VolumeTexture(velocitySource, width, height);

      // load in the shaders
      this->sliceRenderer = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/slice.glsl"))
              }}
                });
      this->depthUniformLocation = this->sliceRenderer.getLocation("renderDepth");
      this->interpolationUniformLocation = this->sliceRenderer.getLocation("interpolation");
      this->sliceRenderer.activate();
      glUniform1i(this->sliceRenderer.getLocation("fromDensity"), 0);
      glUniform1i(this->sliceRenderer.getLocation("toDensity"), 1);

      this->mainScreen.setActive(false);

      this->solver.setVelocityOverlay(this->velocityOverlay);
      this->density.setDensityOverlay(this->densityOverlay);
    }

    struct DrawOp
    {
      virtual void operator()() = 0;
      virtual void activate() = 0;

      virtual ~DrawOp()
      {
      };
    };

    struct NormalDrawOp : public DrawOp
    {
      OpenGL::Mesh* mesh;

      NormalDrawOp(OpenGL::Mesh* mesh) :
        mesh(mesh)
      {
      }

      void activate()
      {
        std::cout << "normal draw op" << std::endl;
      }

      void operator()()
      {
        mesh->draw();
      }
    };

    struct DebugDrawOp : public DrawOp
    {
      OpenGL::Shader* shader;
      OpenGL::Shader* origShader;
      GLint location;
      GLint value;
      const OpenGL::VolumeTexture* tex;
      OpenGL::Mesh* mesh;
      std::string textureName;

      DebugDrawOp(OpenGL::Shader* shader, OpenGL::Shader* origShader,
          GLint location, GLint value,
          const OpenGL::VolumeTexture* tex,
          OpenGL::Mesh* mesh,
          const std::string& texName):
        shader(shader), origShader(origShader),
        location(location), value(value),
        tex(tex), mesh(mesh),
        textureName(texName)
      {
      }

      void activate()
      {
        std::cout << "debug draw op, texture " << this->textureName << ", channel " << this->value << std::endl;
        glActiveTexture(GL_TEXTURE3);
        this->tex->bind();
        glActiveTexture(GL_TEXTURE0);
      }

      void operator()()
      {
        shader->activate();
        glUniform1i(location, value);
        mesh->draw();
        origShader->activate();
      }
    };

    char Demo::operator()()
    {
      // main loop variables
      bool closing = false;
      char reset = 0; // don't reset by default

      // for polling the window events
      sf::Event event;

      // clock stuff
      typedef std::chrono::high_resolution_clock Clock;
      Clock clock;
      std::chrono::milliseconds NScycleTime(50); // cycles 5 x a second
      Clock::time_point nextCycleTime = clock.now() + NScycleTime;

      // set up the debug shader
      this->mainScreen.setActive(true);
      OpenGL::Shader debugShader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/square.vert"))}},
          {GL_FRAGMENT_SHADER, {
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/version.glsl")),
            OpenGL::Shader::terminate(loadFile("src/ns/shaders/debug.glsl"))
              }}
                });
      debugShader.activate();
      glUniform1i(debugShader.getLocation("values"), 3);
      glUniform1i(debugShader.getLocation("channel"), 0);
      glActiveTexture(GL_TEXTURE3);
      this->solver.tempVelocity.bind();
      glActiveTexture(GL_TEXTURE0);
      this->sliceRenderer.activate();

      std::vector<std::unique_ptr<DrawOp> > drawModes;
      // normal
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new NormalDrawOp(&this->squareMesh)));
      // debug normal (slow update)
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 0, this->density.getDensityTextures()[0], &this->squareMesh, std::string("normal"))));

      // pressure near end of calculation x
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 0, &this->solver.intermediates[0], &this->squareMesh, std::string("intermediates[0]"))));
      // pressure near end of calculation y
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 1, &this->solver.intermediates[0], &this->squareMesh, std::string("intermediates[0]"))));
      // pressure near end of calculation z
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 2, &this->solver.intermediates[0], &this->squareMesh, std::string("intermediates[0]"))));

      // vel near end of calculation x
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 0, &this->solver.intermediates[1], &this->squareMesh, std::string("intermediates[1]"))));
      // vel near end of calculation y
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 1, &this->solver.intermediates[1], &this->squareMesh, std::string("intermediates[1]"))));
      // vel near end of calculation z
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 2, &this->solver.intermediates[1], &this->squareMesh, std::string("intermediates[1]"))));

      // post-projected velocity x
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 0, &this->solver.velocityTexture, &this->squareMesh, std::string("velocity"))));
      // post-projected velocity y
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 1, &this->solver.velocityTexture, &this->squareMesh, std::string("velocity"))));
      // post-projected velocity z
      drawModes.emplace_back(std::unique_ptr<DrawOp>(new DebugDrawOp(&debugShader, &this->sliceRenderer, debugShader.getLocation("channel"), 2, &this->solver.velocityTexture, &this->squareMesh, std::string("velocity"))));

      auto currentDrawMode = drawModes.begin();

      this->mainScreen.setActive(false);

      // main loop
      while (!closing)
      {
        // draw to screen
        this->mainScreen.setActive(true);
        auto dim = this->solver.getSize();

        // check if the user has closed the screen
        while (this->mainScreen.pollEvent(event))
        {
          if (event.type == sf::Event::Closed)
          {
            // flag the demo to close next iteration
            closing = true;
            break;
          }
          else if (event.type == sf::Event::MouseButtonPressed)
          {

            // put a full cell at the point where the mouse is pressed
            static const std::vector<std::array<GLfloat, 4> > whiteColumn(dim[2], {{1.0, 1.0, 1.0, 1.0}});
            static const std::vector<std::array<GLfloat, 4> > blankColumn(dim[2], {{0.0, 0.0, 0.0, 0.0}});

            // work out which cell to fill
            auto windowSize = this->mainScreen.getSize();
            GLuint mouseX = event.mouseButton.x * dim[0] / windowSize.x;
            GLuint mouseY = dim[1] - (event.mouseButton.y * dim[1] + 1) / windowSize.y;

            // upload the cell change
            auto upload = event.mouseButton.button == sf::Mouse::Left ? whiteColumn.data() : blankColumn.data();
            glActiveTexture(GL_TEXTURE0 + 2);
            glTexSubImage3D(GL_TEXTURE_3D, 0, 
                mouseX, mouseY, 0,
                1, 1, dim[2],
                GL_RGBA, GL_FLOAT, upload);
            glActiveTexture(GL_TEXTURE0);
          }
          else if (event.type == sf::Event::Resized)
          {
            // ensure the viewport is set up correctly
            auto windowSize = this->mainScreen.getSize();
            glViewport(0,0, windowSize.x, windowSize.y);
          }
          else if (event.type == sf::Event::KeyPressed && sf::Keyboard::Key::A == event.key.code)
          {
            // cycle through the draw modes
            if (drawModes.end() == ++currentDrawMode)
            {
              currentDrawMode = drawModes.begin();
            }

            (*currentDrawMode)->activate();
          }
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // upload the interpolation amount
        glUniform1f(this->interpolationUniformLocation,
            static_cast<GLfloat>(std::chrono::duration_cast<std::chrono::milliseconds>((clock.now() - nextCycleTime) + NScycleTime).count()) / NScycleTime.count());
        // bind the density textures to units 0 and 1
        auto densities = this->density.getDensityTextures();

        glActiveTexture(GL_TEXTURE0 + 0);
        densities[0]->bind();

        glActiveTexture(GL_TEXTURE0 + 1);
        densities[1]->bind();

        // render through the textures slice by slice
        this->squareMesh.setUp();
        for (GLfloat depth = 0.5f / dim[2]; depth <= 1.0f - 0.5f / dim[2] / 2; depth += this->sliceWidth)
        {
          // upload the current depth
          glUniform1f(this->depthUniformLocation, depth);

          // render
          (**currentDrawMode)();
        }
        this->squareMesh.tearDown();

        this->mainScreen.display();
        
        GLenum errorCode;
        while (GL_NO_ERROR != (errorCode = glGetError()))
        {
          std::cerr << "got an error: " << errorCode << std::endl;
        }

        this->mainScreen.setActive(false);

        // if time has passed, cycle the solver
        while (clock.now() > nextCycleTime)
        {
          this->solver();
          this->density();

          nextCycleTime += NScycleTime;
        }
      }

      return reset;
    }
  }
}
