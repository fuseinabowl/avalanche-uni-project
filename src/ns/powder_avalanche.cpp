#include "powder_avalanche.h"
#include "loadfile.h"

namespace Avalanche
{
  namespace NS
  {
    PowderSimulator::PowderSimulator(const Landscape::Landscape& landscape, 
        unsigned width, unsigned height, unsigned depth,
        float viscosity) :
      solver(width, height, depth, viscosity),
      powder(solver),
      landscape(&landscape),
      forceField(std::vector<std::array<GLfloat, 4> >(width * height * depth, {{0.0, 0.0, 0.0, 0.0}}), width, height)
    {
      // construct the shaders and set them up (texture bindings, get the layer uniform locations)
      this->renderer = OpenGL::Shader({
          {GL_VERTEX_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/powder.vert"))}},
          {GL_FRAGMENT_SHADER, {OpenGL::Shader::terminate(loadFile("src/ns/shaders/powder.frag"))}}
            });

      // turn off the context so no one else can mess with it
      this->context.setActive(false);
    }

    void PowderSimulator::operator()()
    {
      // turn on my own context
      this->context.setActive(true);

      // calculate the force field
      // get the latest powder field
      this->powder.getDensityTextures()[1]->bind();

      // turn off the context
      this->context.setActive(false);

      // now get the simulators to perform an iteration
      this->solver();
      this->powder();
    }

    void PowderSimulator::render(const OpenGL::Camera& cam, GLfloat interpolation) const
    {
      // do not use my own context, this should write to whatever the caller has active
      // activate the shader
      this->renderer.activate();

      // upload the camera matrices
      cam.uploadModelView(rendererModelView);
      cam.uploadProjection(rendererProjection);
      cam.uploadNormal(rendererNormal);

      // activate the textures
      auto powderTextures = this->powder.getDensityTextures();
      glActiveTexture(GL_TEXTURE0);
      powderTextures[0]->bind();
      glActiveTexture(GL_TEXTURE1);
      powderTextures[1]->bind();

      // interpolation upload
      glUniform1f(this->rendererInterpolation, interpolation);

      // work out when to start and finish the layers
      // run through as many layers as you want, interpolating between the start and finish calculated just before
    }
  }
}
