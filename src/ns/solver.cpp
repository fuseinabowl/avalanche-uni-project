#include "solver.h"

#include <vector>
#include <utility>
#include <iostream>
#include "openGL.h"
#include "loadfile.h"
#include <complex>

// the start point for texture units
// TODO redesign this so that you can have multiple solvers and won't ever break
// other openGL things using texture units

namespace Avalanche
{
  namespace NS
  {
    const OpenGL::VolumeTexture& Solver::getBlankTexture()
    {
      static const std::vector<std::array<GLfloat, 4> > data(1, {{0.0}});
      static const OpenGL::VolumeTexture blankTexture(data, 1, 1);
      return blankTexture;
    }

    Solver::Solver(unsigned width, unsigned height, unsigned depth, float viscosity) :
      width(width), height(height), depth(depth),
      velocityOverlayField(&Solver::getBlankTexture())
    {
      // explicitly set the contex active just to remind maintainers that
      // all work is done in a separate context
      this->context.setActive(true);

      // set up the viewport
      glViewport(0,0, width, height);

      // set up the framebuffer
      glGenFramebuffers(1, &this->framebufferName);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->framebufferName);

      // set up the textures
      std::vector<std::array<GLfloat, 4> > data(width * height * depth,
        {{0.00001,0.0,0.0,0.0}});
      for (unsigned x = 0; x < width; ++x)
      {
        for (unsigned y = 0; y < height; ++y)
        {
          for (unsigned z = 0; z < depth; ++z)
          {
            data[x + y * width + z * width * height][1] = 0.00000000001 * 
              (x * (width - x)) *
              (y * (height - y)) *
              (z * (depth - z) + 1);
          }
        }
      }
      this->velocityTexture = OpenGL::VolumeTexture(data, width, height);

      data = std::vector<std::array<GLfloat, 4> >(width * height * depth,
          {{0.0,0.0,0.0,0.0}});

      this->tempVelocity = OpenGL::VolumeTexture(data, width, height);
      this->velocityDivergenceTex = OpenGL::VolumeTexture(data, width, height);
      for (unsigned char i = 0; i < 2; ++i)
      {
        this->intermediates[i] = OpenGL::VolumeTexture(data, width, height);
      }

      // set up the shaders
      this->setUpShaders(20, viscosity);
      syncObject = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);

      this->context.setActive(false);
    }

    Solver::~Solver()
    {
      this->context.setActive(true); // so that texture destructors work properly
    }

    // the next solver run should put this texture in a texture unit in the context
    // TODO make this not leak memory
    void Solver::setVelocityOverlay(const OpenGL::VolumeTexture& texture)
    {
      this->velocityOverlayField = &texture;
    }

    const OpenGL::VolumeTexture& Solver::getVelocityTexture() const
    {
      return this->velocityTexture;
    }

    std::array<unsigned, 3> Solver::getSize() const
    {
      return {{this->width, this->height, this->depth}};
    }

    GLfloat Solver::getTimestep() const
    {
      return 1.0;
    }
  }
}
