#ifndef _NS_DEMO_H_
#define _NS_DEMO_H_

#include "solver.h"
#include "openGL.h"
#include <SFML/Window.hpp>
#include "openGL/texture.h"
#include "openGL/VBO.h"
#include "density_advector.h"

namespace Avalanche
{
  namespace NS
  {
    class Demo
    {
      class GlewInitialiser
      {
       public:
        class GlewFailure : public std::exception
        {
          std::string failureString;
         public:
          GlewFailure(GLuint statusCode);
          ~GlewFailure() noexcept;
          const char* what() noexcept;
        };
        GlewInitialiser();
      };
      sf::Window mainScreen;
      GlewInitialiser init;
      OpenGL::UnitSquare squareMesh;

      Solver solver;
      DensityAdvector density;

      // apply this texture to the density texture every frame
      OpenGL::VolumeTexture densityOverlay;
      OpenGL::VolumeTexture velocityOverlay;

      GLfloat sliceWidth;

      OpenGL::Shader sliceRenderer;
      GLint depthUniformLocation;
      GLint interpolationUniformLocation;
     public:
      Demo(unsigned width, unsigned height, unsigned depth, int argc, const char** argv);

      // run the demo
      // returns when the demo is closed
      char operator()();
    };
  }
}

#endif
